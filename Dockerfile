FROM node:6.7.0

# cache npm install when package.json hasn't changed
WORKDIR /tmp
ADD package.json package.json
RUN npm install
RUN npm install -g pm2

RUN mkdir /judy
RUN cp -a /tmp/node_modules /judy

WORKDIR /judy
ADD . /judy/

ENV NODE_ENV production
ENV OPTIMIZELY_URL https://cdn.optimizely.com/js/6263440432.js

RUN npm run build

# upload js and css
WORKDIR /judy/build

# go back to /judy
WORKDIR /judy

ENV NODE_PATH "./src"
ENV HOST 127.0.0.1
ENV PORT 8000

EXPOSE 8000
CMD ["pm2", "start", "./bin/server.js", "--no-daemon", "-i", "0"]
