import React from 'react';
import {IndexRoute, Route} from 'react-router';
import {
    App,
    Home,
    About,
    Pricing,
    Faq,
    Terms,
    Contact,
    NotFound,
    Features,
    B2BPortal,
    SalesRepsOnDemand,
    CallCenter,
    WhyOrdercircle,
    QuickbooksIntegration,
    ShopifyIntegration,
    ShipstationIntegration,
    ShipwireIntegration,
    Integrations,
  } from 'containers';

export default () => {
  return (
    <Route path="/" component={App}>
      <IndexRoute component={Home}/>
      <Route path="about" component={About}/>
      <Route path="features" component={Features}/>
      <Route path="pricing" component={Pricing}/>
      <Route path="faq" component={Faq}/>
      <Route path="terms" component={Terms}/>
      <Route path="contact" component={Contact}/>

      <Route path="b2b-portal" component={B2BPortal}/>
      <Route path="sales-reps-on-demand" component={SalesRepsOnDemand}/>
      <Route path="wholesale-call-center" component={CallCenter}/>
      <Route path="why-ordercircle" component={WhyOrdercircle}/>

      <Route path="quickbooks-integration" component={QuickbooksIntegration}/>
      <Route path="shopify-integration" component={ShopifyIntegration}/>
      <Route path="shipstation-integration" component={ShipstationIntegration}/>
      <Route path="shipwire-integration" component={ShipwireIntegration}/>
      <Route path="integrations" component={Integrations}/>

      <Route path="*" component={NotFound} status={404} />
    </Route>
  );
};
