import React, {Component, PropTypes} from 'react';
import ReactDOM from 'react-dom/server';
import serialize from 'serialize-javascript';
import Helmet from 'react-helmet';
import config from 'config';

/**
 * Wrapper component containing HTML metadata and boilerplate tags.
 * Used in server-side code only to wrap the string output of the
 * rendered route component.
 *
 * The only thing this component doesn't (and can't) include is the
 * HTML doctype declaration, which is added to the rendered output
 * by the server.js file.
 */
export default class Html extends Component {
  static propTypes = {
    assets: PropTypes.object,
    component: PropTypes.node,
    store: PropTypes.object
  };

  render() {
    const {assets, component, store} = this.props;
    const content = component ? ReactDOM.renderToString(component) : '';
    const head = Helmet.rewind();

    let intercom = null;
    if(config.isProduction) {
      intercom = [
        <script dangerouslySetInnerHTML={{
            __html: `
              window.intercomSettings = {
                app_id: "yvzxc4p3"
              };
            `
          }}/>,
        <script dangerouslySetInnerHTML={{
            __html: `
              (function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/yvzxc4p3';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()
            `
          }}/>
      ];
    }


    return (
      <html lang="en-us">
        <head>
          {head.base.toComponent()}
          {head.title.toComponent()}
          {head.meta.toComponent()}
          {head.link.toComponent()}
          {head.script.toComponent()}

          <link rel="icon" href="/dist/favicon.ico" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          {/* styles (will be present only in production with webpack extract text plugin) */}
          {Object.keys(assets.styles).map((style, key) =>
            <link href={assets.styles[style]} key={key} media="screen, projection"
                  rel="stylesheet" type="text/css" charSet="UTF-8"/>
          )}

          {/* (will be present only in development mode) */}
          {/* outputs a <style/> tag with all bootstrap styles + App.scss + it could be CurrentPage.scss. */}
          {/* can smoothen the initial style flash (flicker) on page load in development mode. */}
          {/* ideally one could also include here the style for the current page (Home.scss, About.scss, etc) */}
          { Object.keys(assets.styles).length === 0 ? <style dangerouslySetInnerHTML={{__html: require('../theme/bootstrap.config.js') + require('../containers/App/App.scss')._style}}/> : null }

          {/* HubSpot Tracking Code */}
          <script type="text/javascript" dangerouslySetInnerHTML={{
            __html: `
              (function(d,s,i,r) {
                if (d.getElementById(i)){return;}
                var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
                n.id=i;n.src='//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/2216089.js';
                e.parentNode.insertBefore(n, e);
              })(document,"script","hs-analytics",300000);
            `
          }} />

          <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>

          {/* Google Analytics snippet */}
          <script
              dangerouslySetInnerHTML={{
                __html: `(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
                ga('create', 'UA-55326856-1', 'auto');
                `
              }}
              charSet="UTF-8"
            />

          {/* FB Pixel */}
          <script
            dangerouslySetInnerHTML={{
              __html: `!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','https://connect.facebook.net/en_US/fbevents.js');
                fbq('init', '1168989586456874');
                fbq('track', "PageView");
              `
            }}/>
          <noscript dangerouslySetInnerHTML={{
            __html: `<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1168989586456874&ev=PageView&noscript=1"`}}
          />

          {/* Inspectlet Snippt */}
          <script type="text/javascript" id="inspectletjs" dangerouslySetInnerHTML={{
              __html: `
                window.__insp = window.__insp || [];
                __insp.push(['wid', 729763727]);
                (function() {
                function ldinsp(){if(typeof window.__inspld != "undefined") return; window.__inspld = 1; var insp = document.createElement('script'); insp.type = 'text/javascript'; insp.async = true; insp.id = "inspsync"; insp.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cdn.inspectlet.com/inspectlet.js'; var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(insp, x); };
                setTimeout(ldinsp, 500); document.readyState != "complete" ? (window.attachEvent ? window.attachEvent('onload', ldinsp) : window.addEventListener('load', ldinsp, false)) : ldinsp();
                })();
              `
          }}/>

          {/* Intercom Snippet */}
          { intercom }

          <script src="https://use.fontawesome.com/90f245edfe.js"></script>
          <link rel="stylesheet" type="text/css" href="https://cloud.typography.com/7343534/7552552/css/fonts.css" />

        </head>
        <body>
          <div id="content" dangerouslySetInnerHTML={{__html: content}}/>
            {
              process.env.OPTIMIZELY_URL &&
              <script src={process.env.OPTIMIZELY_URL} />
            }
          <script dangerouslySetInnerHTML={{__html: `window.__data=${serialize(store.getState())};`}} charSet="UTF-8"/>
          <script src={assets.javascript.vendor} charSet="UTF-8"/>
          <script src={assets.javascript.app} charSet="UTF-8"/>

          <script type="text/javascript"
            dangerouslySetInnerHTML={{
              __html: `/* <![CDATA[ */ var google_conversion_id = 920977585; var google_custom_params = window.google_tag_params; var google_remarketing_only = true;/* ]]> */`}} />
          <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
          </script>
          <noscript dangerouslySetInnerHTML={{__html: `<div style="display:inline;"><img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/920977585/?value=0&amp;guid=ON&amp;script=0"/></div>` }} />

        </body>
      </html>
    );
  }
}
