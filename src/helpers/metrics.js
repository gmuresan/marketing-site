import config from 'config';

export default {
  pageViewEvent: 'pageLoad',
  vendors: [
    {
      api: {
        name: 'OrderCircle',
        pageView(eventName) {
          if (!config.isProduction) return true;

          if (eventName !== 'pageLoad') {
            return analytics.page(...arguments);
          }

          return ga('send', 'pageview', location.pathname);
        },
        track(eventName, params) {
          if (!config.isProduction) return true;

          return ga('send', {
            hitType: 'event',
            eventCategory: eventName,
            eventAction: params.action || 'click',
            eventLabel: params.label || 'Interactions'
          });
        },
        user(user) {
          return new Promise(resolve => {
            resolve({
              user
            });
          });
        }
      }
    }
  ]
};
