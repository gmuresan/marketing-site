import React, { Component, PropTypes } from 'react';
import { LinkContainer } from 'react-router-bootstrap';
import Row from 'react-bootstrap/lib/Row';
import Grid from 'react-bootstrap/lib/Grid';
import Col from 'react-bootstrap/lib/Col';
import Button from 'react-bootstrap/lib/Button';
import Image from 'react-bootstrap/lib/Image';

const styles = require('./style.scss');

export default class ScreenshotInfoPanel extends Component {

  static propTypes = {
    img: PropTypes.string.isRequired,
    children: PropTypes.array.isRequired,
    rightImage: PropTypes.bool,
    buttonHref: PropTypes.string,
    buttonText: PropTypes.string,
    transparent: PropTypes.bool,
  };

  static defaultProps = {
    buttonText: 'Learn More',
    transparent: false,
  };

  render() {
    const imagePush = this.props.rightImage * 7;
    const imageWidth = 5;
    const imageOffset = 0;

    const textPull = this.props.rightImage * 4;
    const textOffset = 0;
    let textWidth = 6;
    if (this.props.rightImage) {
      textWidth = 6;
    }

    let button = null;
    if(this.props.buttonHref) {
      button = (
        <LinkContainer to={this.props.buttonHref}>
          <Button
            className={styles.button}
            bsStyle="primary"
            bsSize="lg"
            href="/b2b-portal"
            data-metrics-event-name="Home B2B Portal Learn More Button Clicked"
          >
            {this.props.buttonText}
          </Button>
        </LinkContainer>
      );
    }

    const containerStyle = {};
    if(this.props.transparent) {
      containerStyle.backgroundColor = 'transparent';
    }

    return (
      <Grid fluid>
        <Row style={containerStyle} className={styles.container}>
          <Col className={styles.imageContainer} mdOffset={imageOffset} md={imageWidth} mdPush={imagePush}>
            <Image src={this.props.img} className={styles.screenshot} responsive />
          </Col>
          <Col mdOffset={textOffset} md={textWidth} mdPull={textPull}>

            <div className={styles.childContainer}>
              {this.props.children}

              {button}
            </div>
          </Col>
        </Row>
      </Grid>
    );
  }

}

