import React from 'react';

const styles = require('./style.scss');

const Ribbon = ({ children, position = 'topLeft', color = 'blue' }) => (
  <div className={`${styles.ribbon} ${styles[position]} sticky ${styles[color]} shadow`}>{children}</div>
);

export default Ribbon;
