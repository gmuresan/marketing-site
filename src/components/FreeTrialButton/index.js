import React, { Component, PropTypes } from 'react';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Button from 'react-bootstrap/lib/Button';
import Image from 'react-bootstrap/lib/Image';
import Well from 'react-bootstrap/lib/Well';
import Modal from 'react-bootstrap/lib/Modal';
import querystring from 'querystring';
import Cookies from 'js-cookie';

const styles = require('./style.scss');
const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


export default class FreeTrialButton extends Component {
  state = {
    company: '',
    email: '',
    invalidCompany: false,
    invalidEmail: false
  };

  onChange(key, event) {
    const state = this.state;
    state[key] = event.target.value;
    this.setState(state);

    if(this.state.invalidCompany || this.state.invalidEmail) {
      this.validateInput();
    }
  }

  validateInput() {
    const validEmail = emailRegex.test(this.state.email);
    const validCompany = this.state.company.length > 0;

    this.setState({invalidCompany: !validCompany, invalidEmail: !validEmail});

    return validEmail && validCompany;
  }

  submitFormToHubspot() {
    if(this.validateInput()) {
      const data = querystring.stringify({
        'email': this.state.email,
        'company': this.state.company,
        'hs_context': JSON.stringify({
          'hutk': Cookies.get('hubspotutk'),
          'pageUrl': window.location.href,
          'pageName': document.getElementsByTagName('title')[0].innerHTML
        })
      });

      const thiz = this;
      fetch('https://forms.hubspot.com/uploads/form/v2/2216089/e9b1cbed-c6ee-4261-82f3-865bca9dceed', {
        method: 'POST',
        mode: 'no-cors',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Content-Length': data.length
        },
        body: data
      }).then(function(response) {
        window.location.href = thiz.getSignupUrl();
      }).catch(function(ex) {
        window.location.href = thiz.getSignupUrl();
      });
    }
  }

  getSignupUrl() {
    return `/signup?email=${this.state.email}&company=${this.state.company}`;
  }

  render() {
    let emailExtraClass = null;
    if(this.state.invalidEmail) {
      emailExtraClass = styles.inputError;
    }

    let companyExtraClass = null;
    if(this.state.invalidCompany) {
      companyExtraClass = styles.inputError;
    }

    return (
      <span>
        <input
          type="text"
          name="company_name"
          placeholder="Company"
          value={this.state.company}
          className={` ${styles.signupInput} ${companyExtraClass}`}
          onChange={this.onChange.bind(this, 'company')} />
        <input type="text"
          name="email"
          placeholder="Email"
          className={`${styles.signupInput} ${emailExtraClass}`}
          value={this.state.email}
          onChange={this.onChange.bind(this, 'email')} />
        <Button
          className={styles.signupButton}
          bsStyle="primary"
          bsSize="lg"
          data-metrics-event-name="Home Hero Signup Button Clicked"
          onClick={this.submitFormToHubspot.bind(this)}
        >
          Start Free Trial
        </Button>
        <div className={styles.signupSubtitle}>
          14 day trial - No credit card required
        </div>
      </span>
    );
  }

}

