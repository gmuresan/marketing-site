import React from 'react';
import Row from 'react-bootstrap/lib/Row';
import Grid from 'react-bootstrap/lib/Grid';
import Col from 'react-bootstrap/lib/Col';
import Button from 'react-bootstrap/lib/Button';

const styles = require('./style.scss');

const powernet = require('../../../static/images/logos/powernet-logo.png');
const thinkjeryky = require('../../../static/images/logos/think-jerky-logo.png');
const taos = require('../../../static/images/logos/taos-mountain-bar-logo.png');
const boxcar = require('../../../static/images/logos/boxcar-coffee-logo.png');
const elevated = require('../../../static/images/logos/elevated-logo.png');

const images = [powernet, thinkjeryky, taos, boxcar, elevated];

const ExistingCustomers = () => (
  <div className={styles.container}>
      <div>
        <h2 className={styles.header}>Our tools have helped hundreds of brands increase their wholesale volume</h2>
        <h3 className={styles.subtitle}>We love our customers and your success is vital to our business</h3>
      </div>
      <div className={styles.logoRow}>
          {
            images.map((image, index) => (
              <img src={image} key={index} className={styles.logo} />
            ))
          }
      </div>
  </div>
);


export default ExistingCustomers;

