import React, { Component, PropTypes } from 'react';

const styles = require('./style.scss');

export default class HeroPanel extends Component {
  static propTypes = {
    children: PropTypes.object,
    green: PropTypes.bool,
    blue: PropTypes.bool,
  };

  render() {
    let colorOverlay = styles.blackOverlay;
    if (this.props.green) {
      colorOverlay = styles.greenOverlay;
    } else if (this.props.blue) {
      colorOverlay = styles.blueOverlay;
    }

    return (
      <div className={styles.hero}>
        <div className={styles.overlay} />
        <div className={colorOverlay} />
        <div className={styles.pageHeader}>
          {this.props.children}
        </div>
      </div>
    );
  }
}

