import React, { Component, PropTypes } from 'react';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Button from 'react-bootstrap/lib/Button';
import Image from 'react-bootstrap/lib/Image';
import Well from 'react-bootstrap/lib/Well';
import Modal from 'react-bootstrap/lib/Modal';
import fetch from 'whatwg-fetch';

const personImg = require('../../../static/images/name-icon.png');
const emailImg = require('../../../static/images/mail-icon.png');
const phoneImg = require('../../../static/images/telephone-icon.png');
const companyImg = require('../../../static/images/company-icon.png');

const styles = require('./style.scss');

const fields = [
  {
    placeholder: 'Your Name',
    icon: personImg,
    type: 'text',
    className: styles.nameImage,
    prop: 'name',
  },
  {
    placeholder: 'Email Address',
    icon: emailImg,
    type: 'text',
    className: null,
    prop: 'email',
  },
  {
    placeholder: 'Company Name',
    icon: companyImg,
    type: 'text',
    className: null,
    prop: 'company',
  },
  {
    placeholder: 'Telephone Number',
    icon: phoneImg,
    type: 'tel',
    className: null,
    prop: 'phone',
  },

];

export default class CallbackForm extends Component {
  static propTypes = {
    green: PropTypes.bool
  };

  state = {
    submitted: false,
    name: '',
    email: '',
    company: '',
    phone: '',
  };

  componentDidMount() {
    hbspt.forms.create({
      css: '',
      portalId: '2216089',
      formId: 'f75d9076-e772-41c5-b530-a3f33d9d907f',
      target: '.' + styles.form,
      submitButtonClass: styles.button + ' btn btn-primary',
      cssClass: styles.form,
    });
  }

  submitForm(event) {
    event.preventDefault();
    const state = this.state;
    const email = this.state.email;
    const phone = this.state.phone;
    const company = this.state.company;
    const name = this.state.name;
  }

  handleChange(key, event) {
    const value = event.target.value;
    const state = this.state;
    state[key] = value;
    this.setState(state);
  }

  render() {
    let buttonColorStyle = null;
    if(this.props.green) {
      buttonColorStyle = styles.green;
    }

    let form = null;
    if(!this.state.submitted) {
      form = (
        <form onSubmit={(evt) => this.submitForm(evt)}>
          {
            fields.map((field, index) => (
              <div className={`input-group ${styles.inputGroup}`} key={index}>
                <span className={`input-group-addon ${styles.addon}`}>
                  <Image src={field.icon} className={field.className} />
                </span>
                <input value={this.state[field.prop]} onChange={(evt) => this.handleChange(field.prop, evt)} type={field.type} className={`form-control ${styles.input}`} placeholder={field.placeholder} />
              </div>
            ))
          }

          <Button
            className={`${styles.button} ${buttonColorStyle}`}
            bsStyle="primary"
            bsSize="lg"
            type="submit"
            data-metrics-event-name="Callback Form Submit Button Clicked"
          >
            Sign Up
          </Button>
        </form>
      );
    }

    return (
      <Grid fluid>
        <h2 className={styles.callbackHeader}>Let us call you back to answer your questions</h2>
        <Row>
          <Col lgOffset={4} lg={4} mdOffset={2} md={8}>
          <div className={styles.form} />
          </Col>
        </Row>

      </Grid>
    );
  }
}

