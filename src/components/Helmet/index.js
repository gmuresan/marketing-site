import React, { Component, PropTypes } from 'react';
import HelmetMeta from 'react-helmet';

export default class Helmet extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
  };

  render() {
    const description = this.props.description;
    const meta = [{ name:'description', content:description }];

    return (
      <HelmetMeta title={this.props.title} meta={meta}/>
    );
  }
}
