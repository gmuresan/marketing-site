import React from 'react';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Button from 'react-bootstrap/lib/Button';

import Ribbon from '../Ribbon';

const plans = [
  {
    name: 'Free',
    cost: 0,
    orders: 'Up To 3 Orders / month',
    customers: 'Unlimited',
    staff: 0,
    products: 'Unlimited',
    payment: 'Included',
    domain: 'Choose a custom subdomain',
    whitelabel: 'Custom Branding',
    support: 'Email Support',
    dam: ' ',
    onboarding: '',
    leads: 0
  },
  {
    name: 'Standard',
    cost: 199,
    orders: 'Up To 50 Orders / month',
    customers: 'Unlimited',
    staff: -1,
    products: 'Unlimited',
    payment: 'Included',
    domain: 'Choose a custom subdomain',
    whitelabel: 'Custom Branding',
    support: 'Phone support',
    isPopular: true,
    dam: '',
    onboarding: '',
    leads: 0
  },
  {
    name: 'Unlimited',
    cost: 299,
    orders: 'Unlimited Orders',
    customers: 'Unlimited',
    staff: -1,
    products: 'Unlimited',
    payment: 'Included',
    domain: 'Use your own domain',
    whitelabel: 'Whitelabeled',
    support: '7 Day Priority Phone Support',
    isPopular: false,
    dam: 'Dedicated Account Manager',
    onboarding: 'Guided Onboarding and Store Setup',
    leads: 0
  },
];

const styles = require('./style.scss');

const PricingTable = () => (
  <Row className={styles.table}>
    <Col md={12}>
      <Row>
        {
          plans.map(function(plan) {
            let leads = null;
            if (plan.leads === 1) {
              leads = <div className={styles.spec}><span className={styles.variable}>{plan.leads} Qualified Wholesale Lead / month</span></div>;
            } else if (plan.leads > 1) {
              leads = <div className={styles.spec}><span className={styles.variable}>{plan.leads} Qualified Wholesale Leads / month</span></div>;
            }

            let staff = null;
            if (plan.staff === 0) {
              staff = <div className={styles.spec}>Staff Accounts Not Included</div>;
            } else if (plan.staff === -1) {
              staff = <div className={styles.spec}><span className={styles.variable}>Unlimited</span> Staff Accounts</div>;
            }

            return (
              <Col key={plan.name} md={4} className={`${styles.column} ${plan.isPopular && styles.popular}`}>
                {plan.isPopular && <Ribbon>Most Popular</Ribbon>}
                <div className={styles.name}>{plan.name}</div>
                <div className={styles.quantity}>
                  <span className={styles.dollar}>$</span>
                  <span className={styles.price}>{plan.cost}</span>
                  <span className={styles.period}>/month</span>
                </div>
                <div className={styles.specs}>
                  <div className={styles.spec}>
                    <span className={styles.variable}>{plan.orders}</span>
                  </div>
                  <div className={styles.spec}>
                    <span className={styles.variable}>{plan.customers}</span> customers
                  </div>
                  {staff}
                  <div className={styles.spec}>
                    <span className={styles.variable}>{plan.products}</span> products
                  </div>
                  <div className={styles.spec}>
                    <span className={styles.variable}>{plan.payment}</span> payment processing
                  </div>
                  <div className={styles.spec}>
                    <span className={styles.variable}>{plan.whitelabel}</span>
                  </div>
                  <div className={styles.spec}>
                    {plan.domain}
                  </div>
                  <div className={styles.spec}>
                    {plan.support}
                  </div>
                  <div className={styles.spec}>
                    <span className={styles.variable}>{plan.dam}</span>
                  </div>
                  <div className={styles.spec}>
                    <span className={styles.variable}>{plan.onboarding}</span>
                  </div>
                  {leads}
                </div>
                <div className={styles.start_trial}>
                  <Button
                    bsStyle="info"
                    href="/signup"
                    data-metrics-event-name="Pricing Plan Button Clicked"
                    data-metrics-pricing-plan={plan.name}
                  >
                    Start Trial
                  </Button>
                </div>
              </Col>
            );
          })
        }
      </Row>
    </Col>
    <Col md={12} className="text-center margin-md-top">
      <strong>All of our plans begin with a free 14 day trial with no credit card required.</strong><br />
      <strong>If OrderCircle doesn't work for you in the first 30 days after your trial, no problem. We'll give you a full refund.</strong>
    </Col>
  </Row>
);


export default PricingTable;
