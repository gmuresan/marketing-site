import React, { Component } from 'react';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Button from 'react-bootstrap/lib/Button';
import Image from 'react-bootstrap/lib/Image';
import Well from 'react-bootstrap/lib/Well';
import Modal from 'react-bootstrap/lib/Modal';


const styles = require('./style.scss');

export default class ScheduleDemo extends Component {

  render() {
    return (
      <div className={styles.container}>
        <h2 className={styles.header}>Need more information?</h2>
        <Button
          className={styles.scheduleDemoButton}
          bsStyle="primary"
          bsSize="lg"
          href="https://calendly.com/ordercircle"
          data-metrics-event-name="Home Schedule Demo Button Clicked"
          target="_blank"
        >
          Request a Live Demo
        </Button>
      </div>
    );
  }
}


