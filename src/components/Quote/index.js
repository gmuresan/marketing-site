import React from 'react';

const styles = require('./style.scss');

const Quote = ({ text, author }) => (
  <div>
    <div className={`${styles.quote} text-muted`}>
      {text}
      <div className={styles.arrowDown}>
        <div className={styles.arrow} />
        <div className={styles.border} />
      </div>
    </div>
    <div className={styles.author}>
      <img src={author.image} width="71" height="71" className={styles.pic} alt="testimonial1" />
      <div className={styles.name}>{author.name}</div>
      <div className={styles.company}>
        {author.company}
        <br />
        <strong><em>{author.info}</em></strong>
      </div>
    </div>
  </div>
);


export default Quote;
