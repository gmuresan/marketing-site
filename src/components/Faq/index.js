import React from 'react';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';

const Faq = () => (
  <div>
    <Row>
      <Col md={6}>
        <h4><strong>Do I need a credit card to signup?</strong></h4>
        <p>
          No, we do not require a credit card in order to signup for a trial of OrderCircle. If you choose to continue using the software after the trial is up, you will be prompted for a credit card at that time.
        </p>
      </Col>
      <Col md={6}>
        <h4><strong>What happens if I go over my plan's order limits?</strong></h4>
        <p>
          No problem, we won't shut you down. You will be automatically upgraded to the next plan tier.
        </p>
      </Col>
    </Row>
    <Row>
      <Col md={6}>
        <h4><strong>How is the monthly order limit calculated?</strong></h4>
        <p>
          The monthly order limit counts all your orders across all customers. For example, if you have 10 customers and they each order twice a month, you would have 20 orders per month.
        </p>
      </Col>
      <Col md={6}>
        <h4><strong>Do all plans come with all the features?</strong></h4>
        <p>
          Yes! All OrderCircle plans come with the same wholesale order management features. The only feature we reserve for the higher plans is custom domains. All our plans offer full access to our native integrations.
        </p>
      </Col>
    </Row>
    <Row>
      <Col md={6}>
        <h4><strong>Can you help me import my products/customers/data?</strong></h4>
        <p>
          Of course :) OrderCircle has built-in CSV imports that allow you to do most of your bulk setup yourself, but we are always glad to offer personal handholding free of charge if you get stuck. Just drop us an email at help@ordercircle.com and we'll get you all setup ASAP.
        </p>
      </Col>
      <Col md={6}>
        <h4><strong>What platforms does OrderCircle integrate with?</strong></h4>
        <p>
          We have native integrations with Quickbooks Online, Shopify, and ShipStation.
        </p>
      </Col>
    </Row>
    <Row>
      <Col md={6}>
        <h4><strong>How does credit card processing work on OrderCircle?</strong></h4>
        <p>
          OrderCircle allows your customers to leave a credit card on file to pay for their orders. When the customer's card is charged, we deposit the money directly into your bank account 3 days later. OrderCircle charges a 3% processing fee for credit card orders, but we absorb all processing fees - so that's all you pay to accept cards.
        </p>
      </Col>
      <Col md={6}>
        <h4><strong>How can I get support?</strong></h4>
        <p>
          Depending on your plan, we offer either email or phone support. All users can email us at help@ordercircle.com to open a ticket. Users whose plans offer telephone support can call us at the number listed on your "Subscription" page. We have a team of US-based folks who try to answer all emails within 24 hours.
        </p>
      </Col>
    </Row>
    <Row>
      <Col md={6}>
        <h4><strong>Can I change the currency for my store?</strong></h4>
        <p>
          We support every currency in the world. When you sign up, you can choose which currency you want to do business in. If anything ever changes, you can switch the currency in your store settings page.
        </p>
      </Col>
      <Col md={6}>
        <h4><strong>What if my wholesale business has unique requirements that OrderCircle doesn't exactly meet?</strong></h4>
        <p>
          If you're looking for something customized to accomodate your business's requirements, OrderCircle will gladly assist you. Contact us with information about your wholesale/B2B needs.
        </p>
      </Col>
    </Row>
  </div>
);

export default Faq;
