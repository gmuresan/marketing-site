import React, { Component, PropTypes } from 'react';
import { LinkContainer } from 'react-router-bootstrap';
import Row from 'react-bootstrap/lib/Row';
import Grid from 'react-bootstrap/lib/Grid';
import Col from 'react-bootstrap/lib/Col';
import Button from 'react-bootstrap/lib/Button';
import Image from 'react-bootstrap/lib/Image';

const styles = require('./style.scss');

export default class ImageInfoPanel extends Component {

  static propTypes = {
    img: PropTypes.string.isRequired,
    children: PropTypes.array.isRequired,
    rightImage: PropTypes.bool,
    buttonHref: PropTypes.string,
    containerClass: PropTypes.object,
    buttonText: PropTypes.string.isRequired,
    transparent: PropTypes.bool,
  };

  static defaultProps = {
    buttonText: 'Learn More',
    transparent: false
  };

  render() {
    const imagePush = this.props.rightImage * 5;
    const imageWidth = 3;
    const imageOffset = 2;

    const textPull = this.props.rightImage * 3;
    const textOffset = 0;
    let textWidth = 6;
    if (this.props.rightImage) {
      textWidth = 5;
    }

    const childContainerClass = this.props.rightImage ? styles.rightImageChildContainer : styles.leftImageChildContainer;

    let button = null;
    if(this.props.buttonHref) {
      button = (
          <Button
            className={styles.button}
            href={this.props.buttonHref}
            target="_blank"
            bsStyle="primary"
            bsSize="lg"
            data-metrics-event-name="Home B2B Portal Learn More Button Clicked"
          >
            {this.props.buttonText}
          </Button>
      );

      if(this.props.buttonHref[0] === '/') {
        button = (
          <LinkContainer to={this.props.buttonHref}>
              {button}
          </LinkContainer>
        );
      }
    }

    const containerStyle = {};
    if(this.props.transparent) {
      containerStyle.backgroundColor = 'transparent';
    }

    return (
      <Grid fluid className={this.props.containerClass}>
        <Row style={containerStyle} className={styles.container}>
          <Col mdPush={imagePush} mdOffset={imageOffset} md={imageWidth}>
            <Image src={this.props.img} className={styles.image} responsive />
          </Col>
          <Col mdPull={textPull} mdOffset={textOffset} md={textWidth}>

              <div className={childContainerClass}>
                {this.props.children}

                {button}
              </div>

          </Col>
        </Row>
      </Grid>
    );
  }

}


