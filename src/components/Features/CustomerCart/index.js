import React, { Component } from 'react';
import Grid from 'react-bootstrap/lib/Grid';
import Tab from 'react-bootstrap/lib/Tab';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Image from 'react-bootstrap/lib/Image';
import SingleScreenshotFeatures from 'components/Features/SingleScreenshotFeatures';

const customerCartImg = require('../../../../static/images/screenshots/customercart.png');

// const styles = require('./style.scss');

const features = [
  {
    heading: 'Item Catalogs',
    paragraphs: [
      {
        text: 'Let your customers see your product list, including any items exclusive to wholesale customers. You can curate your lists to show certain items to certain customers, or even hide some items from special customers.'
      }
    ]
  },
  {
    heading: 'Customer Accounts',
    paragraphs: [
      {
        text: 'Create separate accounts for each wholesale customer. You must approve each customer, so you only do business with the retailers that you trust.'
      }
    ]
  },
  {
    heading: 'Self Service Order Tracking',
    paragraphs: [
      {
        text: 'Allow your customer to manage and track their orders have they have been placed. They will have the ability to:',
        bullets: [
          'Track shipments',
          'Make payments',
          'View invoices',
          'View their order history'
        ]
      }
    ]
  },
  {
    heading: '1 Click Reordering',
    paragraphs: [
      {
        text: 'For customers that order the same items, recreate an order with 1 click. This will save you and your customer valuable time every week.'
      }
    ]
  }
];

export default class CustomerCart extends Component {
  render() {
    return (
      <div>
        <SingleScreenshotFeatures
          heading="Accept B2B Orders Online"
          subheading="Eliminate the need for phone calls, emails, and fax by letting your wholesale customers place orders online"
          features={features}
          screenshot={customerCartImg} />
      </div>

    );
  }
}

