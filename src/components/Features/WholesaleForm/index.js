import React, { Component } from 'react';
import SingleScreenshotFeatures from 'components/Features/SingleScreenshotFeatures';

const img = require('../../../../static/images/screenshots/wholesalerequest.png');

const features = [
  {
    heading: 'Form Customization',
    paragraphs: [
      {
        text: 'Customize the form by adding fields for any information you need from potential customers.'
      }
    ]
  },
  {
    heading: 'Approve or Deny Requests',
    paragraphs: [
      {
        text: 'You control who can log in and start placing orders. As wholesaler, you want to make sure only legitimate retailers have access to your discounted wholesale prices. When a request comes in you evaluate the business to make sure they are someone you want to do business with you. If you approve the request, their account is automatically created and they can start placing orders right away.'
      }
    ]
  },
  {
    heading: 'Ability To Hide The Request Form',
    paragraphs: [
      {
        text: 'If you already have enough customers, or you aren\'t ready to expand, you can hide the request form so you aren\'t spammed with unwanted inquiries.'
      }
    ]
  },
];

export default class WholesaleForm extends Component {
  render() {
    return (
      <div>
        <SingleScreenshotFeatures
          heading="Request A Wholesale Account Form"
          subheading="New customers can apply for wholesale accounts directly on your website"
          features={features}
          screenshot={img} />
      </div>

    );
  }
}

