import React, { Component } from 'react';
import SingleScreenshotFeatures from 'components/Features/SingleScreenshotFeatures';

const customerCartImg = require('../../../../static/images/screenshots/shipstation.png');

const features = [
  {
    heading: 'Shipment Creation',
    paragraphs: [
      {
        text: 'OrderCircle will automatically create a shipment for you in Shipstation when you approve an order in OrderCircle.'
      }
    ]
  },
  {
    heading: 'Sync Shipments Back To OrderCircle',
    paragraphs: [
      {
        text: 'When you ship the order through Shipstation, we\'ll automatically pull that information back to OrderCircle. Your customers will be able to see the shipment status along with the tracking number, and we\'ll add this information to their invoices and email notifications.'
      }
    ]
  },
];

export default class Shipstation extends Component {
  render() {
    return (
      <div>
        <SingleScreenshotFeatures
          heading="Shipstation Integration"
          subheading="Use Shipstation to automatically sync your shipment tracking with your order management"
          features={features}
          screenshot={customerCartImg} />
      </div>

    );
  }
}


