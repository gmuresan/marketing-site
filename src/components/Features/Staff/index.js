import React, { Component } from 'react';
import SingleScreenshotFeatures from 'components/Features/SingleScreenshotFeatures';

const img = require('../../../../static/images/screenshots/staffcommissions.png');

const features = [
  {
    heading: 'Place Orders On Behalf Of Customers',
    paragraphs: [
      {
        text: 'For customers that don\'t want to login and place orders online, your sales staff can create orders for them. The customer will still receive notifications and invoices if they want.'
      }
    ]
  },
  {
    heading: 'Unique Logins',
    paragraphs: [
      {
        text: 'Create separate user accounts for each member of your staff. We don\'t charge extra for each account, so you can create as many accounts as you need.'
      }
    ]
  },
  {
    heading: 'Commission Tracking',
    paragraphs: [
      {
        text: 'Configure separate commission rates for each sales rep and we\'ll track the commission earned by each rep.'
      }
    ]
  },
  {
    heading: 'Permission Management',
    paragraphs: [
      {
        text: 'Limit the access of each sales rep by specifying if they are allowed to create orders, customers, items. You can also configure OrderCircle to only allow sales reps to see and interact with customers assigned to them.'
      }
    ]
  },
];

export default class Staff extends Component {
  render() {
    return (
      <div>
        <SingleScreenshotFeatures
          heading="Unlimited Staff Accounts"
          subheading="Create as many staff accounts as you want for no extra charge. Assign sales reps to your customers and track commissions."
          features={features}
          screenshot={img} />
      </div>

    );
  }
}


