import React, { Component } from 'react';
import SingleScreenshotFeatures from 'components/Features/SingleScreenshotFeatures';

const img = require('../../../../static/images/screenshots/customeritemview.png');

const features = [
  {
    heading: 'International Currencies',
    paragraphs: [
      {
        text: 'Display prices and accept payments in the currency of your choice.'
      }
    ]
  },
  {
    heading: 'Weight Units',
    paragraphs: [
      {
        text: 'Use pounds, kilograms, or stones for your item weights and shipping weight.'
      }
    ]
  },
  {
    heading: 'Tax Support',
    paragraphs: [
      {
        text: 'Customize your tax rates by setting a store wide tax rate, or set a different tax rate for each customer. We support tax code synchronization through QuickBooks.'
      }
    ]
  }];

export default class Localization extends Component {
  render() {
    return (
      <div>
        <SingleScreenshotFeatures
          heading="Localization"
          subheading="Customize your store for the country your are selling in"
          features={features}
          screenshot={img} />
      </div>

    );
  }
}

