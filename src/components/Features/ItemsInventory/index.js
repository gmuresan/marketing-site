import React, { Component } from 'react';
import Grid from 'react-bootstrap/lib/Grid';
import Tab from 'react-bootstrap/lib/Tab';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Image from 'react-bootstrap/lib/Image';
import SingleScreenshotFeatures from 'components/Features/SingleScreenshotFeatures';

const img = require('../../../../static/images/screenshots/edititem.png');

const features = [
  {
    heading: 'Item Catalog',
    paragraphs: [
      {
        text: 'Don\'t mess around with paper and PDF line sheets anymore. Upload your items to OrderCircle and let your customers browse your updated catalog online.'
      }
    ]
  },
  {
    heading: 'Inventory Management',
    paragraphs: [
      {
        text: 'With OrderCircle you can keep track of your available inventory for every SKU. Inventory will be deducted in real time as orders are created. If you use Shopify, we\'ll keep your inventory in sync with your Shopify store.'
      }
    ]
  },
  {
    heading: 'Catalog Visibility Controls',
    paragraphs: [
      {
        text: 'If you have special items that are only availble to certain customers, you can choose which items you want to show/hide for every single customers. You can also create special lists like "Summer Specials" or "Best Selling Items".'
      }
    ]
  },
  {
    heading: 'Bulk Upload',
    paragraphs: [
      {
        text: 'Don\'t spend time creating every single item individually. You can import your current linesheet and create all your items in seconds. Or import all your items from your Shopify store instantly.'
      }
    ]
  }
];

export default class ItemsInventory extends Component {
  render() {
    return (
      <div>
        <SingleScreenshotFeatures
          heading="Item and Inventory Management"
          subheading="Keep your item catalog organized with OrderCircle so you and your customers always know which items are available in real time"
          features={features}
          screenshot={img} />
      </div>

    );
  }
}

