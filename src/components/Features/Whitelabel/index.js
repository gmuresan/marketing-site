import React, { Component } from 'react';
import SingleScreenshotFeatures from 'components/Features/SingleScreenshotFeatures';

const img = require('../../../../static/images/screenshots/customdomain.png');

const features = [
  {
    heading: 'Custom Domain (URL)',
    paragraphs: [
      {
        text: 'Use your existing domain to host your new wholesale store. Most brands use wholesale.[your_brand].com, but you can use any URL you want.'
      }
    ]
  },
  {
    heading: 'White Label',
    paragraphs: [
      {
        text: 'Your customers won\'t see any references to OrderCircle on their website. It will look like you personally created the store specially for your customers.'
      }
    ]
  }];

export default class Whitelabel extends Component {
  render() {
    return (
      <div>
        <SingleScreenshotFeatures
          heading="Customer Branding"
          subheading="Your online store will appear professionally made specifically for your brand."
          features={features}
          screenshot={img} />
      </div>

    );
  }
}

