import React, { Component } from 'react';
import SingleScreenshotFeatures from 'components/Features/SingleScreenshotFeatures';

const img = require('../../../../static/images/screenshots/editcustomer.png');

const features = [
  {
    heading: 'Price Tiers And Price Lists',
    paragraphs: [
      {
        text: 'Assign special prices for each customer individually or for groups of customers. Set a retail price for each item, then specify if a customer gets a percentage discount or a fixed discount. You can override the price for each individual item as needed.'
      }
    ]
  },
  {
    heading: 'Payment Terms',
    paragraphs: [
      {
        text: 'B2B customers can get different payment terms that aren\'t available to consumers. Specify a customer gets net 30-day terms or if payment is due on the day of the order and anything in between.'
      }
    ]
  },
  {
    heading: 'Store Credit',
    paragraphs: [
      {
        text: 'Reward loyal customers with store credit that automatically gets applied to subsequent orders.'
      }
    ]
  },
];

export default class WholesaleTerms extends Component {
  render() {
    return (
      <div>
        <SingleScreenshotFeatures
          heading="Wholesale Terms"
          subheading="B2B commerce plays by different rules from B2C"
          features={features}
          screenshot={img} />
      </div>

    );
  }
}

