import React, { Component } from 'react';
import SingleScreenshotFeatures from 'components/Features/SingleScreenshotFeatures';

const customerCartImg = require('../../../../static/images/screenshots/quickbooks.png');

const features = [
  {
    heading: 'Automatic Invoice Creation',
    paragraphs: [
      {
        text: 'OrderCircle automatically creates an invoice for you when an order is approved. The invoice is automatically populated with the customer, the items, and all the costs associated with the order.'
      }
    ]
  },
  {
    heading: 'Customer and Item Linking',
    paragraphs: [
      {
        text: 'Link your QuickBooks items and customers with your items and customers in OrderCircle. You can choose to link each item individually or just use a generic line like "Sales" for all your items.'
      }
    ]
  },
  {
    heading: 'Tax Support',
    paragraphs: [
      {
        text: 'If you track taxes in QuickBooks, OrderCircle will automatically calculate the total tax payment for each order and add it to the invoice.'
      }
    ]
  },
  {
    heading: 'Quickbooks Payments Synchronization',
    paragraphs: [
      {
        text: 'If you use QuickBooks Payments, the payment data will automatically sync back to OrderCircle for each order. The order will get marked as paid and the invoice will be updated to reflect the payment.'
      }
    ]
  },
];

export default class Quickbooks extends Component {
  render() {
    return (
      <div>
        <SingleScreenshotFeatures
          heading="QuickBooks Integration"
          subheading="Use QuickBooks with OrderCircle to keep your accounting organized and up to date"
          features={features}
          screenshot={customerCartImg} />
      </div>

    );
  }
}

