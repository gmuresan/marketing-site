import React, { Component } from 'react';
import SingleScreenshotFeatures from 'components/Features/SingleScreenshotFeatures';

const img = require('../../../../static/images/screenshots/addpayment.png');

const features = [
  {
    heading: '3 Day Rolling Payouts',
    paragraphs: [
      {
        text: 'Get paid out directly to your bank account 3 days after the payment is submitted. OrderCircle takes a 3% fee on payments and forwards the rests to you. We don\'t charge any hidden fees or minimum monthly fees.'
      }
    ]
  },
  {
    heading: 'Set Different Payment Terms For Each Customer',
    paragraphs: [
      {
        text: 'With OrderCircle you have the option of configuring payment terms for each customer individually. We understand that some wholesale customers need to pay 100% upfront while others may get net 30 day terms. You charge their card automatically when payment is due, or charge their card manually when you\'re ready. You can even let the customer pay when they\'re ready, whether that\'s the day the order was placed or a month later.'
      }
    ]
  },
  {
    heading: 'Record Manual Payments',
    paragraphs: [
      {
        text: 'For customers that pay by check or COD, you can record a manual payment for each order. This helps you keep all your payment records in once place so your accounting stays organized and up to date at all times.'
      }
    ]
  },
  {
    heading: '3rd Party Payment Processing Integrations',
      paragraphs: [
        {
          text: 'We also integrate with established payment processors if you have an existing relationship you want to maintain. We currently have Authorize.net, PayPayl Payflow Pro, and Quickbooks Payments.'
        }
      ]
  },
];

export default class Payments extends Component {
  render() {
    return (
      <div>
        <SingleScreenshotFeatures
          heading="Accept Credit Card Payments Online"
          subheading="Use our built-in payment processor to accept all major credit card payments"
          features={features}
          screenshot={img} />
      </div>

    );
  }
}

