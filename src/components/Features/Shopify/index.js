import React, { Component } from 'react';
import SingleScreenshotFeatures from 'components/Features/SingleScreenshotFeatures';

const customerCartImg = require('../../../../static/images/screenshots/shopify.png');

const features = [
  {
    heading: 'Import Your Products From Shopify',
    paragraphs: [
      {
        text: 'Save time by importing your existing products from your Shopify store. Once the products are imported you can edit properties that are specific to wholesale, like price tiers, shipping weight, MOQ, and minimum order quantity.'
      }
    ]
  },
  {
    heading: 'Inventory Synchronization',
    paragraphs: [
      {
        text: 'OrderCircle will keep your inventory in sync with Shopify. If an item gets purchased from your OrderCircle store, we\'ll deduct that quantity from Shopify, and vice-versa.'
      }
    ]
  },
];

export default class Shopify extends Component {
  render() {
    return (
      <div>
        <SingleScreenshotFeatures
          heading="Shopify Integration"
          subheading="Keep your products and inventory in sync with your Shopify store"
          features={features}
          screenshot={customerCartImg} />
      </div>

    );
  }
}

