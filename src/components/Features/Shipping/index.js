import React, { Component } from 'react';
import SingleScreenshotFeatures from 'components/Features/SingleScreenshotFeatures';

const img = require('../../../../static/images/screenshots/shipments.png');

const features = [
  {
    heading: 'Order Tracking',
    paragraphs: [
      {
        text: 'You and your customers can track the status of their order at any time. Use ShipStation to automatically sync tracking numbers or input tracking numbers manually for your customers.'
      }
    ]
  },
  {
    heading: 'Fill In Shipping Costs Later',
    paragraphs: [
      {
        text: 'With large orders you don\'t always know your shipping costs when the order is placed. With OrderCircle you can create orders with TBD shipping costs and fill in the cost later.'
      }
    ]
  },
  {
    heading: 'Multiple Shipping Addresses',
    paragraphs: [
      {
        text: 'Save an unlimited number of shipping addresses for each customer. Select the correct address when the order is created.'
      }
    ]
  }];

export default class Shipping extends Component {
  render() {
    return (
      <div>
        <SingleScreenshotFeatures
          heading="Shipment Tracking"
          subheading="Shipping features built for wholesale ordering"
          features={features}
          screenshot={img} />
      </div>

    );
  }
}

