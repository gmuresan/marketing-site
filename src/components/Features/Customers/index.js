import React, { Component } from 'react';
import SingleScreenshotFeatures from 'components/Features/SingleScreenshotFeatures';

const img = require('../../../../static/images/screenshots/customerlist.png');

const features = [
  {
    heading: 'Track Customer Behavior',
    paragraphs: [
      {
        text: 'OrderCircle tells you which items each customer is purchasing and how much revenue you\'re getting from each customer. You\'ll be able to predict when customers need to reorder and reach out to them proactively.'
      }
    ]
  },
  {
    heading: 'Real-time notifications for your customers',
    paragraphs: [
      {
        text: 'You can configure OrderCircle to notify your customers whenever their order status changes. Don\'t waste time answering inquiries about when the order is getting shipped and if a payment is still needed.'
      }
    ]
  },
  {
    heading: 'Bulk Upload',
    paragraphs: [
      {
        text: 'Save time by uploading your customers in minutes. We would be glad to assist you if you need help.'
      }
    ]
  },
  {
    heading: 'Multiple Shipping/Billing Addresses',
    paragraphs: [
      {
        text: 'Save time by storing all your customers shipping and billing addresses under one account.'
      }
    ]
  }
];

export default class Customers extends Component {
  render() {
    return (
      <div>
        <SingleScreenshotFeatures
          heading="Customer Management"
          subheading="Create a separate customer account for each wholesale customer and allow only trusted retailers to purchase from you"
          features={features}
          screenshot={img} />
      </div>

    );
  }
}

