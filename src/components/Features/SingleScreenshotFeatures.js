import React, { Component, PropTypes } from 'react';
import Grid from 'react-bootstrap/lib/Grid';
import Tab from 'react-bootstrap/lib/Tab';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Image from 'react-bootstrap/lib/Image';

const styles = require('./style.scss');

export default class SingleScreenshotFeatures extends Component {

  static propTypes = {
    heading: PropTypes.string.isRequired,
    subheading: PropTypes.string,
    screenshot: PropTypes.string.isRequired,
    features: PropTypes.array.isRequired
  };

  renderFeatureParagraphs(feature) {
    return (
      <div>
        {
          feature.paragraphs.map((paragraph, index) => (
            this.renderParagraph(paragraph, index)
          ))
        }
      </div>
    );
  }

  renderParagraph(paragraph, index) {
    let bullets = null;
    if (paragraph.bullets) {
      bullets = (
        <ul>
          {
            paragraph.bullets.map((bullet, bulletIndex) => (
              <li key={bulletIndex}>
                {bullet}
              </li>
            ))
          }
        </ul>
      );
    }

    return (
      <div key={index}>
        <p>
          {paragraph.text}
        </p>
        {bullets}
      </div>
    );
  }

  render() {
    return (
      <div>
        <Row>
          <Col mdOffset={0} md={12}>
          <h2 className={styles.header}>
            {this.props.heading}
          </h2>
          <h3 className={styles.subheading}>
            {this.props.subheading}
          </h3>
          </Col>
        </Row>
        <Row className={styles.screenshotRow}>
          <Col mdOffset={0} md={11}>
            <Image className={styles.screenshot} src={this.props.screenshot} responsive />
          </Col>
        </Row>
        <Col mdOffset={0} md={11}>
          {
            this.props.features.map((feature, index) => (
              <Row key={index}>
                <h3>{feature.heading}</h3>
                {this.renderFeatureParagraphs(feature)}
              </Row>
            ))
          }
        </Col>
      </div>

    );
  }
}

