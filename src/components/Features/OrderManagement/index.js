import React, { Component } from 'react';
import Grid from 'react-bootstrap/lib/Grid';
import Tab from 'react-bootstrap/lib/Tab';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Image from 'react-bootstrap/lib/Image';
import SingleScreenshotFeatures from 'components/Features/SingleScreenshotFeatures';

const img = require('../../../../static/images/screenshots/orderlist.png');

const features = [
  {
    heading: 'Order Workflow',
    paragraphs: [
      {
        text: 'Orders can be in different states depending on which stage of the workflow they are in:',
        bullets: [
          'Pending',
          'Approved',
          'Partially Shipped',
          'Fully Shipped',
          'Paid'
        ]
      }
    ]
  },
  {
    heading: 'Realtime Notifications',
    paragraphs: [
      {
        text: 'Customize notifications for you and your customers so you can get emails when an order:',
        bullets: [
          'Gets created',
          'Is approved',
          'Gets shipped',
          'Payment is recorded',
        ]
      }
    ]
  },
  {
    heading: 'Order Reporting',
    paragraphs: [
      {
        text: 'Export a detailed, customizable report of your orders so you can track the performance of your sales reps and see which items are getting sold.'
      }
    ]
  }
];

export default class OrderManagement extends Component {
  render() {
    return (
      <div>
        <SingleScreenshotFeatures
          heading="Manage Your Wholesale Order In One Place"
          subheading="OrderCircle keeps track of all your orders so you always know the status of your business"
          features={features}
          screenshot={img} />
      </div>

    );
  }
}


