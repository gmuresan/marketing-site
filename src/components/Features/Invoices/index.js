import React, { Component } from 'react';
import SingleScreenshotFeatures from 'components/Features/SingleScreenshotFeatures';

const img = require('../../../../static/images/screenshots/invoice.png');

const features = [
  {
    heading: 'Beautiful PDF Invoices',
    paragraphs: [
      {
        text: 'Automatically generate and send PDF invoices to your customers when an order is approved.'
      }
    ]
  },
  {
    heading: 'Packing Slips For Shipments',
    paragraphs: [
      {
        text: 'Eliminate confusion by including a printed packing slip with each order. Your customer will know exactly which items were included and if any items are arriving in a separate shipment.'
      }
    ]
  },
  {
    heading: 'Pick Lists For Your Warehouse',
    paragraphs: [
      {
        text: 'Make your warehouse manager\'s life easier by providing a pick list for the day\'s orders. Pick lists show all the items that need to go out so you know what to grab from your shelves by looking at a single page.'
      }
    ]
  },
];

export default class Invoices extends Component {
  render() {
    return (
      <div>
        <SingleScreenshotFeatures
          heading="Invoicing"
          subheading="Automatically generate order invoices, packing slips, and pick lists."
          features={features}
          screenshot={img} />
      </div>

    );
  }
}

