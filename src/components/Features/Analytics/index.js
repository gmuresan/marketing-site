import React, { Component } from 'react';
import SingleScreenshotFeatures from 'components/Features/SingleScreenshotFeatures';

const img = require('../../../../static/images/screenshots/dashboard.png');

const features = [
  {
    heading: 'Store Dashboard',
    paragraphs: [
      {
        text: 'Get a quick high level view of your store performance so you always know how much you are selling.'
      }
    ]
  },
  {
    heading: 'Sales Staff Performance',
    paragraphs: [
      {
        text: 'Track your sales staff sales to their assigned customers and use OrderCircle to calculate their monthly commissions'
      }
    ]
  },
  {
    heading: 'Customer Analytics',
    paragraphs: [
      {
        text: 'Know how much you are selling to each customer and which items are being sold.'
      }
    ]
  }];

export default class Whitelabel extends Component {
  render() {
    return (
      <div>
        <SingleScreenshotFeatures
          heading="Analytics"
          subheading="Stay up to date on the metrics that are important to your business"
          features={features}
          screenshot={img} />
      </div>

    );
  }
}

