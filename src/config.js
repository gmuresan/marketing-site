require('babel-polyfill');

const title = 'OrderCircle: Wholesale Ordering Software, Online B2B Ordering Software';
const description = 'B2B/Wholesale eCommerce solution for brands and your buyers. Accept wholesale orders online and fulfill the orders with our wholesale order management suite.';

const environment = {
  development: {
    isProduction: false
  },
  production: {
    isProduction: true
  }
}[process.env.NODE_ENV || 'development'];

module.exports = Object.assign({
  host: process.env.HOST || 'localhost',
  port: process.env.PORT,
  apiHost: process.env.APIHOST || 'localhost',
  apiPort: process.env.APIPORT,
  app: {
    title,
    description,
    head: {
      titleTemplate: '%s',

      meta: [
        {name: 'description', content: description},
        {charset: 'utf-8'}
      ]
    }
  },

}, environment);
