import React, { Component } from 'react';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Button from 'react-bootstrap/lib/Button';
import Image from 'react-bootstrap/lib/Image';
import Well from 'react-bootstrap/lib/Well';
import Modal from 'react-bootstrap/lib/Modal';
import Helmet from 'components/Helmet';

import ExistingCustomers from 'components/ExistingCustomers';
import CallbackForm from 'components/CallbackForm';
import ScreenshotInfoPanel from 'components/ScreenshotInfoPanel';
import ImageInfoPanel from 'components/ImageInfoPanel';
import ScheduleDemo from 'components/ScheduleDemo';
import HeroPanel from 'components/HeroPanel';
import FreeTrialButton from 'components/FreeTrialButton';

const cloudImg = require('../../../static/images/cloud.png');
const lightBulbImg = require('../../../static/images/light-bulb.png');
const multipleStaffImg = require('../../../static/images/multiple-staff.png');
const productFocusImg = require('../../../static/images/product-focus.png');
const customerLogosImg = require('../../../static/images/logos/customer-logos-square.png');

const styles = require('./style.scss');

export default class WhyOrdercircle extends Component {

  render() {
    return (
      <div>
        <Helmet
          title="Wholesale E-Commerce Solutions From Experienced B2B Experts"
          description="Increase your B2B sales with our in house wholesale experts. We have years of experience in wholesale and B2C selling that we can put to work for you!"
        />

        <HeroPanel>
          <Grid>
            <Row>
              <Col mdOffset={2} md={8}>
                <div className={`page-header ${styles.pageHeader}`}>
                  <h1>
                    Why OrderCircle?
                    <small>
                      B2B is all about relationships. Technology is great, but the magic happens with the human touch.
                    </small>
                  </h1>
                </div>
                <FreeTrialButton />
              </Col>
            </Row>
          </Grid>
        </HeroPanel>

        <ImageInfoPanel img={cloudImg} rightImage>
          <h2>Save time and money by upgrading your business and moving to the cloud</h2>
          <h3>
            With OrderCircle, you're better organized, get more repeat business and save money by moving from phone and email to the cloud
            <br /><br />
            More and more retailers are expecting to place their orders online so give them the modern eCommerce experience they expect
          </h3>
        </ImageInfoPanel>

        <ImageInfoPanel img={multipleStaffImg} rightImage>
          <h2>Hire fewer in house staff</h2>
          <h3>
            Hire fewer customer service reps and sales reps by outsourcing to OrderCircle
            <br /><br />
            We can be your on-demand staff where you need it, when you need it
            <br /><br />
            Our tools will even make your existing staff more effective and more efficient
          </h3>
        </ImageInfoPanel>

        <ImageInfoPanel img={customerLogosImg} rightImage>
          <h2>OrderCircle is an expert in the B2B e-commerce space</h2>
          <h3>
            We are trusted by hundreds of brands to manage their wholesale and B2B sales channels
            <br /><br />
            Let us use the experience and knowledge we have gained to improve and streamline your wholesale business
          </h3>
        </ImageInfoPanel>

        <ImageInfoPanel img={productFocusImg} rightImage>
          <h2>Focus your time and energy on creating great products</h2>
          <h3>
            At the end of the day, your customers keep coming back because you offer the best products at the best price.
            <br /><br />
            We'll free up more of your time so you can focus on making even better products for the best prices.
          </h3>
        </ImageInfoPanel>

        <ImageInfoPanel img={lightBulbImg} rightImage>
          <h2>An ever evolving platform</h2>
          <h3>
            Our team of wholesale experts is constantly looking for new features and updates for us to buid into our products
            <br /><br />
            We listen to what our customers want. Let us know if there any missing features that are critical to your business and we'll fit it into our roadmap.
          </h3>
        </ImageInfoPanel>

        <div className={styles.callbackContainer}>
          <CallbackForm />
        </div>

      </div>
    );
  }
}


