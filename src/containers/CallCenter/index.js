import React, { Component } from 'react';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Button from 'react-bootstrap/lib/Button';
import Image from 'react-bootstrap/lib/Image';
import Well from 'react-bootstrap/lib/Well';
import Modal from 'react-bootstrap/lib/Modal';
import Helmet from 'components/Helmet';

import ExistingCustomers from 'components/ExistingCustomers';
import CallbackForm from 'components/CallbackForm';
import ScreenshotInfoPanel from 'components/ScreenshotInfoPanel';
import ImageInfoPanel from 'components/ImageInfoPanel';
import ScheduleDemo from 'components/ScheduleDemo';
import HeroPanel from 'components/HeroPanel';
import FreeTrialButton from 'components/FreeTrialButton';

const phoneOrdersImg = require('../../../static/images/sales-reps-phone-orders.png');
const customerServiceImg = require('../../../static/images/sales-reps-customer-service.png');
const usBasedImg = require('../../../static/images/sales-reps-us-based.png');

const styles = require('./style.scss');

export default class CallCenter extends Component {

  render() {
    return (
      <div>
        <Helmet
          title="US Based Customer Service Outsourcing For Wholesalers and Distributors"
          description="Increase retailer satisfaction with a dedicated line to handle wholesale inquiries and support. Our US based reps provides high quality service on your behalf."
        />

        <HeroPanel>
          <Grid>
            <Row>
              <Col md={12}>
                <div className={`page-header ${styles.pageHeader}`}>
                  <h1>
                    US based wholesale customer service call center
                    <small>
                      Outsource your wholesale customer service to OrderCircle's US based call center
                      <br />
                      Cut costs while maintaining your brand identity and customer satisfaction
                    </small>
                  </h1>
                </div>
                <FreeTrialButton />
              </Col>
            </Row>
          </Grid>
        </HeroPanel>

        <ImageInfoPanel img={phoneOrdersImg} rightImage>
          <h2>Many customers still like to call in and speak directly with a human</h2>
          <h3>
            We handle all your wholesale inquiries on your dedicated phone number so you can focus on whats important to your business
            <br /><br />
            We take orders on the phone and input them directy into your wholesale eCommerce portal
          </h3>
        </ImageInfoPanel>

        <ImageInfoPanel img={customerServiceImg}>
          <h2>Focus your time on the most important parts of your business</h2>
          <h3>
            Never pick up your phone again to answer questions about order status, tracking info, and payment terms
            <br /><br />
            We learn all about your business so we can provide high quality customer service to your wholesale customers
          </h3>
        </ImageInfoPanel>

        <ScheduleDemo />

        <ImageInfoPanel img={usBasedImg} rightImage>
          <h2>Completely US based workforce</h2>
          <h3>
            Never worry about receiving sub par service with our vetted call operators that are always based in the United States
            <br /><br />
            Get up to date statistics on number of calls, customer satisfaction, and order volume
            <br /><br />
            Listen in on calls randomly without either participant knowing you are there. Audit your calls so you know your customers are getting the same level service they would get from you.
          </h3>
        </ImageInfoPanel>

        <div className={styles.callbackContainer}>
          <CallbackForm />
        </div>

      </div>
    );
  }
}


