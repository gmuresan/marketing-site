import React, { Component } from 'react';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Button from 'react-bootstrap/lib/Button';
import Image from 'react-bootstrap/lib/Image';
import Well from 'react-bootstrap/lib/Well';
import Modal from 'react-bootstrap/lib/Modal';
import Helmet from 'components/Helmet';

import CallbackForm from 'components/CallbackForm';
import ScreenshotInfoPanel from 'components/ScreenshotInfoPanel';
import ImageInfoPanel from 'components/ImageInfoPanel';
import ScheduleDemo from 'components/ScheduleDemo';
import HeroPanel from 'components/HeroPanel';
import FreeTrialButton from 'components/FreeTrialButton';

const qbLogoSquare = require('../../../static/images/logos/quickbooks-logo-square.png');

const qbOrderSyncImg = require('../../../static/images/qb-order-sync.png');
const qbTrackPaymentsImg = require('../../../static/images/qb-track-payments.png');
const qbLinkCustomersItems = require('../../../static/images/qb-link-customers-items.png');

const styles = require('./style.scss');

export default class QuickbooksIntegration extends Component {

  render() {
    return (
      <div>
        <Helmet
          title="Wholesale Selling With Automated QuickBooks Accounting"
          description="Accept wholesale orders online with an easy to use shopping cart and order management software. Use your existing QuickBooks customers and items to launch your store."
        />

        <HeroPanel green>
          <Grid>
            <Row>
              <Col md={12}>
                <Image src={qbLogoSquare} />
                <div className={`page-header ${styles.pageHeader}`}>
                  <h1>
                    Wholesale order acceptance with automatic QuickBooks invoicing
                    <small>
                      Cut costs and save time when you connect your invoicing with a dedicated order acceptance portal for your wholesale customers
                    </small>
                  </h1>
                </div>
                <FreeTrialButton />
              </Col>
            </Row>
          </Grid>
        </HeroPanel>

        <ImageInfoPanel img={qbOrderSyncImg} rightImage buttonHref="/b2b-portal">
          <h2>Synchronize your wholesale order management with QuickBooks in real-time</h2>
          <h3>
            Take wholesale orders online from your approved customers and use our backend to manage and fulfill the orders
            <br /><br />
            Invoices get automatically created in your QuickBooks account when you approve each order
          </h3>
        </ImageInfoPanel>

        <ImageInfoPanel img={qbLinkCustomersItems} buttonHref="/b2b-portal" buttonText="See All Features">
          <h2>Customers and items are linked between QuickBooks and OrderCircle</h2>
          <h3>
            Save hours each week when you don't need to manually create each invoice
            <br /><br />
            We automatically fill in customer info, line items for your products, and shipping costs, taxes, and order total
          </h3>
        </ImageInfoPanel>

        <ImageInfoPanel img={qbTrackPaymentsImg} rightImage buttonHref="/signup" buttonText="Start Free Trial">
          <h2>Are you using QuickBooks payments?</h2>
          <h3>
            Payments get automatically synchronized to OrderCircle so you can keep track of them in one place
            <br /><br />
            Eliminate unnecessary payment inquiries when your customers can log in and view their own payment history
          </h3>
        </ImageInfoPanel>


        <div className={styles.callbackContainer}>
          <CallbackForm green />
        </div>

      </div>
    );
  }
}


