import React, { Component } from 'react';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Button from 'react-bootstrap/lib/Button';
import Image from 'react-bootstrap/lib/Image';
import Well from 'react-bootstrap/lib/Well';
import Modal from 'react-bootstrap/lib/Modal';
import Helmet from 'components/Helmet';

import CallbackForm from 'components/CallbackForm';
import ScreenshotInfoPanel from 'components/ScreenshotInfoPanel';
import ImageInfoPanel from 'components/ImageInfoPanel';
import ScheduleDemo from 'components/ScheduleDemo';
import HeroPanel from 'components/HeroPanel';
import FreeTrialButton from 'components/FreeTrialButton';

const ssImportManageShipImg = require('../../../static/images/shipstation-import-manage-ship.png');
const ssShippingCostsImg = require('../../../static/images/shipstation-pull-shipping-costs.png');
const ssShipmentDetailsImg = require('../../../static/images/shipstation-pull-shipment-details.png');
const ssExportOrdersImg = require('../../../static/images/shipstation-export-orders.png');

const ssTopLogo = require('../../../static/images/logos/shipstation-logo-small.png');

const styles = require('./style.scss');

export default class ShipstationIntegration extends Component {

  render() {
    return (
      <div>
        <Helmet
          title="Accept Wholesale Orders Online With ShipStation Fulfillment"
          description="Online e-commerce shopping cart for your B2B customers. Orders automatically sync to ShipStation to make shipping and fulfillment easy."
        />

        <HeroPanel>
          <Grid>
            <Row>
              <Col md={12}>
                <Image src={ssTopLogo} />
                <div className={`page-header ${styles.pageHeader}`}>
                  <h1>
                    Fulfill wholesale orders with Shipstation
                    <small>
                      Accept and manage wholesale orders online and eliminate shipping headaches with our OrderCircle and Shipstation integration
                    </small>
                  </h1>
                </div>
                <FreeTrialButton />
              </Col>
            </Row>
          </Grid>
        </HeroPanel>

        <ImageInfoPanel img={ssImportManageShipImg} rightImage>
          <h2>B2B order management and shipping made easy</h2>
          <h3>
            Give your wholesale customers the modern eCommerce experience they expect
            <br /><br />
            Your B2B customer can log in and place orders online in a self service eCommerce portal
            <br /><br />
            Save time and money when your order acceptance is automatically synchronized with your fulfillment and shipping channels
          </h3>
        </ImageInfoPanel>

        <ImageInfoPanel img={ssExportOrdersImg}>
          <h2>Eliminate the need to input each shipment manually</h2>
          <h3>
            Approved wholesale orders automatically get sent to ShipStation with customer and item details
            <br /><br />
            ShipStation will instantly let you compare shipping rates for orders across many different carriers
          </h3>
        </ImageInfoPanel>

        <ImageInfoPanel img={ssShippingCostsImg} rightImage>
          <h2>Automatically invoice your customers for shipping costs</h2>
          <h3>
            Eliminate the back and forth with your customers over shipping costs and fulfillment status
            <br /><br />
            OrderCircle will sync your shipping costs back to your customers and add them to their invoice automatically
          </h3>
        </ImageInfoPanel>

        <ImageInfoPanel img={ssShipmentDetailsImg}>
          <h2>Shipping status updates come back in real time</h2>
          <h3>
            You and your customers will always be able to see the status of their orders
            <br /><br />
            Automatically send out notification emails when shipments are created and tracking status changes
          </h3>
        </ImageInfoPanel>

        <div className={styles.callbackContainer}>
          <CallbackForm />
        </div>

      </div>
    );
  }
}




