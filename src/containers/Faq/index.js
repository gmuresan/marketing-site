import React, { Component } from 'react';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Helmet from 'components/Helmet';

import FaqComponent from 'components/Faq';

export default class Faq extends Component {
  render() {
    return (
      <Grid className="margin-lg-bottom">

        <Helmet
          title="Get Your B2B and Wholesale Sales Questions Answered"
          description="Ask our experience team about B2B and wholesale e-commerce and sales. We can help with e-commerce, customer service, and even how to find new retail customers."
        />

        <Row>
          <Col md={12} className="text-center">
            <h2 className="margin-md-bottom">
              Frequently Asked Questions
            </h2>
          </Col>
          <Col md={12}>
            <FaqComponent />
          </Col>
        </Row>
      </Grid>
    );
  }
}
