import React, { Component } from 'react';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Button from 'react-bootstrap/lib/Button';
import Image from 'react-bootstrap/lib/Image';
import Well from 'react-bootstrap/lib/Well';
import Modal from 'react-bootstrap/lib/Modal';
import Helmet from 'react-helmet';

import Quote from 'components/Quote';

const testimonies = [
  {
    text: 'My wholesale customers used to have to call or email with orders, and I would send them back an invoice I made in Excel. Now I just watch the orders roll in and take action on any that need special attention. OrderCircle saves me a ton of time.',
    author: {
      name: 'Bill D\'Alessandro',
      company: 'NurtureMyBody.com',
      info: 'Bill sells to Joe →',
      image: 'https://ordercircle.com/assets/images/portrait1.png'
    }
  },
  {
    text: 'I order from Bill\'s company twice a month, and I used to have to call each time. Now I place orders myself online, and OrderCircle remembers my usual order, shipping address, and payment terms. Then I get a tracking number automatically. It\'s awesome.',
    author: {
      name: 'Joe Kanter',
      company: 'Owner, Essence Salon and Spa',
      info: '← Joe orders from Bill',
      image: 'https://ordercircle.com/assets/images/portrait2.png'
    }
  }
];

const styles = require('./style.scss');
const businessOwnerImg = require('../../../static/images/smallbizowner2.jpg');

export default class B2BPortal extends Component {
  state = {
    showVideo: false
  };

  render() {
    return (
      <div>
        <Helmet title="Wholesale Ordering Software, Online B2B Ordering Software"/>
        <div className={styles.hero}>
          <div className={styles.overlay} />
          <Grid>
            <Row>
              <Col md={6}>
                <h1>B2B WHOLESALE ORDERING,<br />SIMPLIFIED.</h1>
                <h4>
                  Make it easy for your wholesale customers to place their own orders online - no more phone orders, faxes, or emailed forms.
                </h4>
                <Button
                  bsStyle="secondary"
                  bsSize="lg"
                  href="/signup"
                  data-metrics-event-name="Home Hero Signup Button Clicked"
                >
                  Start your free trial
                </Button>
              </Col>
              <Col md={6}>
                <Image
                  src="https://ordercircle.com/assets/images/ordercircle-video-thumbnail.png"
                  alt="ordercircle video thumbnail"
                  onClick={() => this.setState({showVideo: true})}
                  className="pointer"
                  response
                  data-metrics-event-name="Video Clicked"
                />
              </Col>
            </Row>
          </Grid>
        </div>
        <Grid>
          <Row>
            <Col md={12} className="text-center padding-lg-v">
              <h2>
                Make it easy for wholesale customers to order from you.
              </h2>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <Row>
                <Col md={2} className="text-center margin-lg-top">
                  <Image src="https://ordercircle.com/assets/images/circle-icons/full-color/money.png" alt="money" responsive />
                </Col>
                <Col md={10}>
                  <h4 className="margin-sm-top"><strong>Take orders online, instantly.</strong></h4>
                  <p>
                    No more calling or faxing in orders, wholesale customers can place orders online instantly.
                  </p>
                </Col>
              </Row>
              <Row>
                <Col md={2} className="text-center margin-lg-top">
                  <Image src="https://ordercircle.com/assets/images/circle-icons/full-color/cart.png" alt="shopping-cart" responsive />
                </Col>
                <Col md={10}>
                  <h4 className="margin-sm-top"><strong>Manage customers, easily.</strong></h4>
                  <p>
                    All your customers automatically receive PDF invoices for every order, and can view up-to-the-minute shipping and tracking information online.
                  </p>
                </Col>
              </Row>
              <Row>
                <Col md={2} className="text-center margin-lg-top">
                  <Image src="https://ordercircle.com/assets/images/circle-icons/full-color/dolly.png" alt="warehouse dolly" responsive />
                </Col>
                <Col md={10}>
                  <h4 className="margin-sm-top"><strong>Reordering, made simple.</strong></h4>
                  <p>
                    Customers' credit cards are stored on file, quick 'Reorder these items' functionality and real-time inventory tracking make it simple to reengage your customers to reorder!
                  </p>
                </Col>
              </Row>
            </Col>
            <Col md={6}>
              <Image src={businessOwnerImg} alt="business owner" responsive />
            </Col>
          </Row>
          <Row>
            <Col md={12} className="text-center padding-lg-v">
              <h2>
                Plus Lots of Features Make Your Life Easy
              </h2>
            </Col>
          </Row>
          <Row>
            <Col md={4}>
              <h3>Online Payments</h3>
              <p>
                Process credit card payments online for each order or record manual payments for customers that prefer to by check or bank wire.
              </p>
            </Col>
            <Col md={4}>
              <h3>Integrations you love</h3>
              <p>
                Automatically sync{' '}
                <Image src="https://ordercircle.com/assets/images/circle-icons/full-color/shopify.png" alt="Shopify logo" height="16px" /> Shopify for products and inventory levels,{' '}
                <Image src="https://ordercircle.com/assets/images/circle-icons/full-color/quickbooks.png" alt="QuickBooks logo" height="16px" /> QuickBooks for invoices, line items and payment status, and{' '}
                <Image src="https://ordercircle.com/assets/images/circle-icons/full-color/shipstation.png" alt="ShipStation logo" height="16px" /> ShipStation for shipping costs and tracking.
              </p>
            </Col>
            <Col md={4}>
              <h3>Multi-Currency Support</h3>
              <p>
                Enables you to assign a specific currency to your store so you can do business in your own currency.
              </p>
            </Col>
          </Row>
          <Row>
            <Col md={4}>
              <h3>Analytics for Your Sales Team</h3>
              <p>
                OrderCircle makes your sales team smarter by showing them who is ordering, what they're ordering, and how often.
              </p>
            </Col>
            <Col md={4}>
              <h3>Commission Tracking</h3>
              <p>
                If you have sales reps, you can automatically track commissions due based on what each rep's accounts actually order.
              </p>
            </Col>
            <Col md={4}>
              <h3>Secure and Private</h3>
              <p>
                Our servers use bank-level encryption to keep your customers' details secure. Each customer only sees their own data.
              </p>
            </Col>
          </Row>
          <Row>
            <Col md={12} className="text-center padding-lg-v">
              <h2>
                Users and Customers love OrderCircle
              </h2>
            </Col>
            {
              testimonies.map((testimony, index) => (
                <Col md={6} key={index}>
                  <Quote {...testimony} />
                </Col>
              ))
            }
          </Row>
          <Row>
            <Col md={12} className="text-center margin-lg-bottom">
              <h2>
                Free trial. No contract. Cancel whenever you want.
              </h2>
            </Col>
          </Row>
          <Row className="margin-lg-v">
            <Col md={12}>
              <Well bsSize="sm">
                <Row>
                  <Col md={8} className="text-right">
                    <h4>
                      If you need a custom wholesale solution, OrderCircle will gladly assist you.
                    </h4>
                  </Col>
                  <Col md={4}>
                    <Button bsStyle="secondary" bsSize="lg" href="mailto:info@ordercircle.com" target="_top">Contact Us</Button>
                  </Col>
                </Row>
              </Well>
            </Col>
          </Row>
        </Grid>
        <Modal
          show={this.state.showVideo}
          onHide={() => this.setState({showVideo: false})}
          bsSize="lg"
        >
          <Modal.Body>
            <iframe
              src="https://player.vimeo.com/video/107058442?byline=0&portrait=0&autoplay=1"
              width="100%"
              height="460"
              frameBorder="0"
              webkitallowfullscreen
              mozallowfullscreen
              allowFullScreen
            />
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}


