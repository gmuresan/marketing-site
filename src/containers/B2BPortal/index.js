import React, { Component } from 'react';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Button from 'react-bootstrap/lib/Button';
import Image from 'react-bootstrap/lib/Image';
import Well from 'react-bootstrap/lib/Well';
import Modal from 'react-bootstrap/lib/Modal';
import Helmet from 'components/Helmet';

import ExistingCustomers from 'components/ExistingCustomers';
import CallbackForm from 'components/CallbackForm';
import ScreenshotInfoPanel from 'components/ScreenshotInfoPanel';
import ImageInfoPanel from 'components/ImageInfoPanel';
import ScheduleDemo from 'components/ScheduleDemo';
import HeroPanel from 'components/HeroPanel';
import FreeTrialButton from 'components/FreeTrialButton';

const dashboardImg = require('../../../static/images/screenshots/dashboard.png');
const orderListImg = require('../../../static/images/screenshots/orderlist-zoomed.png');
const customerListImg = require('../../../static/images/screenshots/customerlist-zoomed.png');
const itemListImg = require('../../../static/images/screenshots/itemlist-zoomed.png');
const customerCartImg = require('../../../static/images/screenshots/customercart.png');

const paymentsImg = require('../../../static/images/payments.png');
const integrationsImg = require('../../../static/images/integrations.png');

const styles = require('./style.scss');

export default class B2BPortal extends Component {
  state = {
    showVideo: false
  };

  render() {
    return (
      <div>
        <Helmet
          title="B2B E-Commerce Platform and Wholesale Order Management Software"
          description="B2B/Wholesale eCommerce order portal for brands and your buyers. Accept wholesale orders online and fulfill the orders with our wholesale order management suite."
        />

        <HeroPanel>
          <Grid>
            <Row>
              <Col md={12}>
                <div className={`page-header ${styles.pageHeader}`}>
                  <h1>
                    Accept And Manage Your Wholesale Orders Online
                    <small>
                      Increase your reorder volume and customer satisfaction by providing an online cart for your B2B customers.
                    </small>
                  </h1>
                </div>
                <div className={styles.videoDiv} onClick={() => this.setState({showVideo: true})} >
                  <i className={`fa fa-play-circle-o fa-4x ${styles.playButton}`} aria-hidden="true"></i>
                  Watch a short video to see how easy it is!
                </div>
                <FreeTrialButton />
              </Col>
            </Row>
          </Grid>
        </HeroPanel>

        <ScreenshotInfoPanel img={dashboardImg} buttonText="See All Features" buttonHref="/features" rightImage>
          <h2>Modern businesses aren't using paper, phone, and fax for wholesale</h2>
          <h3>
            Stay up to date on the status of your wholesale business with our modern dashboard
            <br /><br />
            Knowing your revenue numbers and order volume allows you to know if you're hitting your goals
            <br /><br />
            Maintain your brand identity with a custom domain which allows you and your customers to log in at <b>wholesale.[yourdomain].com</b>
          </h3>
        </ScreenshotInfoPanel>

        <ScreenshotInfoPanel img={customerCartImg}>
          <h2>Let your customers view your catalog and place their own orders</h2>
          <h3>
            There's no need to chase down every customer when it's time to reorder
            <br /><br />
            With our self service portal, your approved customers can log in and reorder with 2 clicks
            <br /><br />
            Watch your business grow as customers keep coming back and reordering
          </h3>
        </ScreenshotInfoPanel>

        <ScreenshotInfoPanel img={itemListImg} buttonText="See All Features" buttonHref="/features" rightImage>
          <h2>Built for wholesale from the ground up</h2>
          <h3>
            Provide each customer with separate pricing, different payment terms, individual shipping channels and customized item catalogs
            <br /><br />
            Generate invoices, print packing slips, and send automatic email notifications to keep your customers up to date
          </h3>
        </ScreenshotInfoPanel>

        <ScreenshotInfoPanel img={customerListImg}>
          <h2>Satisfied customers are 65% more likely to reorder</h2>
          <h3>
            Nurture your customer relationships to keep them happy and coming back
            <br /><br />
            Easily track all of your customers and learn their order habits with our built in customer management tool
            <br /><br />
            Keep your wholesale prices confidential - only approved customers can log in and place orders
          </h3>
        </ScreenshotInfoPanel>

        <ScreenshotInfoPanel img={orderListImg} rightImage>
          <h2>Sales reps can still play a huge role in B2B</h2>
          <h3>
            Each sales rep gets their own user account with no extra fee per account!
            <br /><br />
            Create orders on behalf of your customers when they call in as you transition your customers to the self service portal
            <br /><br />
            Assign a rep to each customer and we'll calculate their commission due each month
          </h3>
        </ScreenshotInfoPanel>

        <ScheduleDemo />

        <ImageInfoPanel img={paymentsImg}>
          <h2>Accept and track payments online</h2>
          <h3>
            Use our built-in credit card processing to have your funds get deposited directly in your bank account
            <br /><br />
            Easily record check payments and wire transfers by adding a manual payment for individual orders
            <br /><br />
            Reuse your existing payments infrastructure with our integrations with Authorize.net, QuickBooks Payments, and PayPal Payflow Pro

          </h3>
        </ImageInfoPanel>

        <ImageInfoPanel img={integrationsImg}
          buttonHref="/integrations"
          rightImage>

          <h2>Integrated with all your existing tools</h2>
          <h3>
            Keep your accounting up to date with QuickBooks
            <br /><br />
            Use ShipStation to track and manage your shipping
            <br /><br />
            Import your existing Shopify store and keep your inventory levels in sync
            <br /><br />
            ShipWire manages your warehouse stock and automatically fulfills orders for you
          </h3>
        </ImageInfoPanel>

        <div className={styles.callbackContainer}>
          <CallbackForm />
        </div>

        <Modal
          show={this.state.showVideo}
          onHide={() => this.setState({showVideo: false})}
          bsSize="lg"
        >
          <Modal.Header closeButton />

          <Modal.Body>
            <iframe
              src="https://player.vimeo.com/video/107058442?byline=0&portrait=0&autoplay=1"
              width="100%"
              height="460"
              frameBorder="0"
              webkitallowfullscreen
              mozallowfullscreen
              allowFullScreen
            />
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

