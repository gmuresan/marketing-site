import React, { Component } from 'react';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Helmet from 'react-helmet';

export default class Terms extends Component {
  render() {
    return (
      <Grid>
        <Helmet title="Terms" />
        <Row>
          <Col md={12}>
            <h3>Terms of Use and Privacy Policy</h3>
          </Col>
          <Col md={12}>
            <h4>In Plain English</h4>
            <p>
            We know most privacy policies and terms of use are so dense that you skip right over them. So we've outlined ours first in plain english, and then posted the full legalese version below. We've done our best to paraphrase, but the "plain english" version is purely for your convenience, the "legalese version" is the legally binding text.
            </p>
            <h5>Rights - In Plain English</h5>
            <p>
            Private data belonging to your or your customers will not be intentionally shown to other users or shared with any third parties.<br />
            Your company's data, including your customer list, your order history, email address, identity, or any other information will not be shared with anyone without your consent.<br />
            Your data will be backed up regularly.<br />
            If there is a significant outage, you will receive a full explanation of the circumstances and what measures will be taken to prevent a recurrence.<br />
            If the site ceases operation, you will receive an opportunity to download your historical invoices and data.<br />
            Any new features that affect privacy will be strictly opt-in.<br />
            You will be treated with respect and discretion.<br />
            You can close your account at any time by emailing our helpdesk (help@ordercircle.com).<br />
            </p>
            <h5>Responsibilities - In Plain English</h5>
            <p>
            You may not make automated requests to the site more frequently than once every thirty seconds.<br />
            You must not abuse the site by knowingly posting malicious code, or links to malicious sites, that could harm other users. If you find a security or privacy bug in OrderCircle, we ask that you please report it to us right away by private email.<br />
            You may not use the site to harass other users or businesses.<br />
            We reserve the right to throttle or close accounts consuming an unreasonable proportion of site resources.<br />
            </p>
            <h4>Full Legalese Version</h4>
            <p>
            OrderCircle Inc ("we" or "us" or "OrderCircle") values its visitors' privacy. This privacy policy is effective [DATE]; it summarizes what information we might collect from a registered user or other visitor ("you"), and what we will and will not do with it.<br /><br />
            Please note that this privacy policy does not govern the collection and use of information by companies that OrderCircle does not control, nor by individuals not employed or managed by OrderCircle. If you visit a Web site that we mention or link to, be sure to review its privacy policy before providing the site with information.
            </p>
            <p>
            It is always up to you whether to disclose personally identifiable information to us, although if you elect not to do so, we reserve the right not to register you as a user or provide you with any products or services. "Personally identifiable information" means information that can be used to identify you as an individual, such as, for example:
            </p>
            <ul>
            <li>your name, company, email address, phone number, billing address, and shipping address</li>
            <li>your OrderCircle user ID and password (if applicable)</li>
            <li>any account-preference information you provide us</li>
            <li>your computer's domain name and IP address, indicating</li>
            <li>where your computer is located on the Internet</li>
            <li>session data for your login session, so that our computer can 'talk' to yours while you are logged in</li>
            </ul>
            <p>
            If you do provide personally identifiable information to us, either directly or through a reseller or other business partner, we will: not sell or rent it to a third party without your permission — although unless you opt out (see below), we may use your contact information to provide you with information we believe you need to know or may find useful, such as (for example) news about our services and products and modifications to the Terms of Service; take commercially reasonable precautions to protect the information from loss, misuse and unauthorized access, disclosure, alteration and destruction; not use or disclose the information except:
            </p>
            <ul>
            <li>as necessary to provide services or products you have ordered;</li>
            <li>in other ways described in this privacy policy or to which you have otherwise consented;</li>
            <li>in the aggregate with other information in such a way so that your identity cannot reasonably be determined (for example, statistical compilations);</li>
            <li>as required by law, for example, in response to a subpoena or search warrant;</li>
            <li>to outside auditors who have agreed to keep the information confidential;</li>
            <li>to a successor organization in the event of a merger, acquisition, bankruptcy, or other sale or disposition of all or a portion of OrderCircle's assets. The successor organization's use and disclosure of your personally-identifiable information will continue to be subject to this privacy policy unless (i) a court orders otherwise, for example a bankruptcy court; or (ii) the successor organization gives you notice that your personally-identifiable information will be subject to the successor organization's own privacy policy, along with an opportunity for you to opt out (which may cause you not to be able to continue to use the OrderCircle software). If you submit personally-identifiable information after such a transfer, that information may be subject to the successor entity's privacy policy;</li>
            <li>as necessary to enforce the Terms of Service;</li>
            <li>as necessary to protect the rights, safety, or property of OrderCircle, its users, or others.</li>
            </ul>
            <p>
            We may collect other information that cannot be readily used to identify you, such as (for example) the domain name and IP address of your computer. We may use this information, individually or in the aggregate, for technical administration of our Web site(s); research and development; customer- and account administration; and to help us focus our marketing efforts more precisely.
            </p>
            <p>
            OrderCircle uses "cookies" to store personal data on your computer. We may also link information stored on your computer in cookies with personal data about specific individuals stored on our servers. If you set up your Web browser (for example, Internet Explorer or Firefox) so that cookies are not allowed, you might not be able to use some or all of the features of our Web site(s).
            </p>
            <p>
            We may store your data on servers provided by third party hosting vendors with whom we have contracted.
            </p>
            <p>
            OrderCircle's operations are located primarily in the United States. If you provide information to us, the information will be transferred out of the European Union (EU) to the United States. By providing personal information to us, you are consenting to its storage and use as described herein.
            </p>
            <p>
            You must be at least 13 years old to use OrderCircle's Web site(s) and service(s). OrderCircle does not knowingly collect information from children under 13. (See the [U.S.] Children’s Online Privacy Protection Act.)
            </p>
            <p>
            We reserve the right to change this privacy policy as we deem necessary or appropriate because of legal compliance requirements or changes in our business practices. If you have provided us with an email address, we will endeavor to notify you, by email to that address, of any material change to how we will use personally identifiable information.
            </p>
            <p>
            If you have questions or comments about OrderCircle's privacy policy, send email to help@ordercircle.com.
            </p>
          </Col>
        </Row>
      </Grid>
    );
  }
}
