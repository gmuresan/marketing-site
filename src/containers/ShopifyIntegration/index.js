import React, { Component } from 'react';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Button from 'react-bootstrap/lib/Button';
import Image from 'react-bootstrap/lib/Image';
import Well from 'react-bootstrap/lib/Well';
import Modal from 'react-bootstrap/lib/Modal';
import Helmet from 'components/Helmet';

import CallbackForm from 'components/CallbackForm';
import ScreenshotInfoPanel from 'components/ScreenshotInfoPanel';
import ImageInfoPanel from 'components/ImageInfoPanel';
import ScheduleDemo from 'components/ScheduleDemo';
import HeroPanel from 'components/HeroPanel';
import FreeTrialButton from 'components/FreeTrialButton';

const shopifyLogoSquare = require('../../../static/images/logos/shopify-logo-square.png');
const shopifyWholesaleImg = require('../../../static/images/shopify-wholesale.png');
const shopifyInventoryImg = require('../../../static/images/shopify-inventory.png');
const shopifyImportProductsImg = require('../../../static/images/shopify-import-products.png');
const shopifyUpdateItemsImg = require('../../../static/images/shopify-item-update.png');


const styles = require('./style.scss');

export default class ShopifyIntegration extends Component {

  render() {
    return (
      <div>
        <Helmet
          title="Sell Wholesale Online Like You Would Sell On Shopify"
          description="Launch an online wholesale store in a few hours and start selling like you would on Shopify. Import your existing products from your Shopify store in seconds."
        />

        <HeroPanel>
          <Grid>
            <Row>
              <Col md={12}>
                <Image src={shopifyLogoSquare} />
                <div className={`page-header ${styles.pageHeader}`}>
                  <h1>
                    Wholesale should be as easy as selling with Shopify
                    <small>
                      Import your existing Shopify store and start selling wholesale in minutes
                    </small>
                  </h1>
                </div>
                <FreeTrialButton />
              </Col>
            </Row>
          </Grid>
        </HeroPanel>

        <ImageInfoPanel img={shopifyWholesaleImg} rightImage buttonText="See All Features" buttonHref="/b2b-portal">
          <h2>Don't limit your business by only selling b2c on Shopify</h2>
          <h3>
            Shopify is great for B2C but it's not made for wholesale ordering
            <br /><br />
            Don't waste your time with incomplete, buggy plugins and complicated discount codes that interfere with your b2c channel
            <br /><br />
            We natively support customer specific pricing and discounts, minimum order values, store credit, and much more
            <br /><br />
            Only preapproved customers can log in and see your wholesale prices
          </h3>
        </ImageInfoPanel>

        <ImageInfoPanel img={shopifyInventoryImg} buttonHref="/b2b-portal">
          <h2>Automatically sync your inventory levels</h2>
          <h3>
            Inventory is deducted from your wholesale store when it decreases on Shopify and vice-versa
            <br /><br />
            When you replenish inventory on Shopify the wholesale levels are automatically synchronized
          </h3>
        </ImageInfoPanel>

        <ImageInfoPanel img={shopifyImportProductsImg} rightImage buttonHref="/signup" buttonText="Start Free Trial">
          <h2>Instantly import your existing SKUs from Shopify</h2>
          <h3>
            Item names, descriptions, images and MSRP prices are automatically populated into your wholesale store
          </h3>
        </ImageInfoPanel>

        <ImageInfoPanel img={shopifyUpdateItemsImg} buttonHref="https://calendly.com/ordercircle" buttonText="Request a Live Demo">
          <h2>Don't waste time updating your items in two places</h2>
          <h3>
            When you make changes to products in Shopify, the same items are updated in OrderCircle
          </h3>
        </ImageInfoPanel>


        <div className={styles.callbackContainer}>
          <CallbackForm green />
        </div>

      </div>
    );
  }
}

