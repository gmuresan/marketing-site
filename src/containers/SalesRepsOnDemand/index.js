import React, { Component } from 'react';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Button from 'react-bootstrap/lib/Button';
import Image from 'react-bootstrap/lib/Image';
import Well from 'react-bootstrap/lib/Well';
import Modal from 'react-bootstrap/lib/Modal';
import Helmet from 'components/Helmet';

import ExistingCustomers from 'components/ExistingCustomers';
import CallbackForm from 'components/CallbackForm';
import ScreenshotInfoPanel from 'components/ScreenshotInfoPanel';
import ImageInfoPanel from 'components/ImageInfoPanel';
import ScheduleDemo from 'components/ScheduleDemo';
import HeroPanel from 'components/HeroPanel';
import FreeTrialButton from 'components/FreeTrialButton';

const wholesaleLeadsImg = require('../../../static/images/sales-reps-leads.png');
const marketingCampaignImg = require('../../../static/images/sales-reps-marketing-campaign.png');
const phoneCallsImg = require('../../../static/images/sales-reps-calling.png');

const styles = require('./style.scss');

export default class SalesRepsOnDemand extends Component {

  render() {
    return (
      <div>
        <Helmet
          title="Automated Campaign to Find and Close New Wholesale Customers"
          description="Use our vast network of retailers across the US to find new customers for your business. Target them with our targeted campaign to close new wholesale accounts."
        />

        <HeroPanel>
          <Grid>
            <Row>
              <Col md={12}>
                <div className={`page-header ${styles.pageHeader}`}>
                  <h1>
                    Find new customers with B2B Sales Reps On Demand
                    <small>
                      Scale up your sales staff month by month as needed to increase sales and find new accounts
                    </small>
                  </h1>
                </div>
                <FreeTrialButton />
              </Col>
            </Row>
          </Grid>
        </HeroPanel>

        <ImageInfoPanel img={wholesaleLeadsImg}>
          <h2>Get access to hundreds of wholesale leads every month</h2>
          <h3>
            Leverage our database of thousands of retail stores all across the United States to find stores that will carry your products
            <br /><br />
            We tailor your leads specifically to your industry to help you reach the right target customers
          </h3>
        </ImageInfoPanel>

        <ImageInfoPanel img={marketingCampaignImg} rightImage>
          <h2>Use automated targeted marketing campaigns to efficiently convert prospects to hot leads</h2>
          <h3>
            Work with your dedicated account manager to develop a campaign that maintains your brand identity
            <br /><br />
            Leads that show interest are vetted by our team for authenticity and we pass them to you for final approval
            <br /><br />
            Stay up to date on the status of your campaign with up to date analytics in your OrderCircle dashboard
          </h3>
        </ImageInfoPanel>

        <ImageInfoPanel img={phoneCallsImg}>
          <h2>Increase reorder volume with outbound calling to your new and existing customers</h2>
          <h3>
            Our US based sales reps use account level analytics determine when a retailer is likely to reorder
            <br /><br />
            We reach out to your customer proactively and encourage them to reorder and we can even sell your new products and special deals
            <br /><br />
            Maintain customer satisfaction levels when each customer is given the attention and service needed to keep them coming back
          </h3>
        </ImageInfoPanel>

        <ScheduleDemo />

        <ImageInfoPanel img={phoneCallsImg}>
          <h2>30 day money back guarantee</h2>
          <h3>
            If you're not satisfied with the service in the first month, we'll give you a full refund with no questions asked
          </h3>
        </ImageInfoPanel>

        <div className={styles.callbackContainer}>
          <CallbackForm />
        </div>

      </div>
    );
  }
}



