import React, { Component } from 'react';
import Grid from 'react-bootstrap/lib/Grid';
import Tab from 'react-bootstrap/lib/Tab';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Nav from 'react-bootstrap/lib/Nav';
import NavItem from 'react-bootstrap/lib/NavItem';
import Helmet from 'components/Helmet';

import CustomerCart from 'components/Features/CustomerCart';
import OrderManagement from 'components/Features/OrderManagement';
import ItemsInventory from 'components/Features/ItemsInventory';
import Customers from 'components/Features/Customers';
import Staff from 'components/Features/Staff';
import Payments from 'components/Features/Payments';
import Shopify from 'components/Features/Shopify';
import Shipstation from 'components/Features/Shipstation';
import Quickbooks from 'components/Features/Quickbooks';
import WholesaleForm from 'components/Features/WholesaleForm';
import WholesaleTerms from 'components/Features/WholesaleTerms';
import Invoices from 'components/Features/Invoices';
import Whitelabel from 'components/Features/Whitelabel';
import Analytics from 'components/Features/Analytics';
import Shipping from 'components/Features/Shipping';
import Localization from 'components/Features/Localization';

const tabs = [
  {title: 'B2B E-Commerce Portal', content: <CustomerCart />},
  {title: 'Order Management', content: <OrderManagement />},
  {title: 'Items and Inventory', content: <ItemsInventory />},
  {title: 'Customer Managment', content: <Customers />},
  {title: 'Staff Accounts', content: <Staff />},
  {title: 'Payments', content: <Payments />},
  {title: 'Wholesale Request Form', content: <WholesaleForm />},
  {title: 'Wholesale Terms', content: <WholesaleTerms />},
  {title: 'Invoicing', content: <Invoices />},
  {title: 'Custom Branding', content: <Whitelabel />},
  {title: 'Analytics', content: <Analytics />},
  {title: 'Shipment Tracking', content: <Shipping />},
  {title: 'Localization', content: <Localization />},
  {title: 'Shopify Integration', content: <Shopify />},
  {title: 'Shipstation Integration', content: <Shipstation />},
  {title: 'QuickBooks Integration', content: <Quickbooks />},
];

const styles = require('./style.scss');

export default class Features extends Component {
  render() {
    return (
      <div>
        <Helmet
          title="E-Commerce Software Made For Wholesale Ordering"
          description="OrderCircle is built for wholesale selling. Most e-commerce solutions only work for B2C. Provide custom price tiers and payment terms for each customer."
        />

        <h2 style={{display: 'none'}} className={styles.header}>We've got lots of features to help you sell more</h2>
        <Tab.Container id="left-tabs-example" defaultActiveKey={0}>
            <Row className="clearfix">
              <Col smOffset={0} sm={3} md={2} xs={4} mdOffset={1}>
                <Nav bsStyle="pills" stacked className={styles.nav}>
                  {
                    tabs.map((tab, index) => (
                      <NavItem key={index} className={styles.navitem} eventKey={index}>
                        { tab.title }
                      </NavItem>
                    ))
                  }
                </Nav>
              </Col>
              <Col md={8} xs={8}>
                <Tab.Content animation>
                  {
                    tabs.map((tab, index) => (
                      <Tab.Pane eventKey={index}>
                        <Col mdOffset={1} md={11}>
                          { tab.content }
                        </Col>
                      </Tab.Pane>
                    ))
                  }
                </Tab.Content>
              </Col>
            </Row>
          </Tab.Container>
        </div>
    );
  }
}

