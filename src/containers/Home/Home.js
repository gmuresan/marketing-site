import React, { Component } from 'react';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Button from 'react-bootstrap/lib/Button';
import Image from 'react-bootstrap/lib/Image';
import Well from 'react-bootstrap/lib/Well';
import Modal from 'react-bootstrap/lib/Modal';
import Helmet from 'components/Helmet';

import ExistingCustomers from 'components/ExistingCustomers';
import CallbackForm from 'components/CallbackForm';
import ScreenshotInfoPanel from 'components/ScreenshotInfoPanel';
import ImageInfoPanel from 'components/ImageInfoPanel';
import ScheduleDemo from 'components/ScheduleDemo';
import HeroPanel from 'components/HeroPanel';
import FreeTrialButton from 'components/FreeTrialButton';

import Quote from 'components/Quote';

const dashboardImg = require('../../../static/images/screenshots/dashboard.png');
const callCenterImg = require('../../../static/images/call-center.png');
const ordercirclePremiumImg = require('../../../static/images/ordercircle-premium.png');
const customSolutionImg = require('../../../static/images/custom-solutions.png');

const styles = require('./style.scss');

export default class Home extends Component {
  state = {
    showVideo: false
  };

  render() {
    return (
      <div>
        <Helmet
          title="Wholesale Selling Tools and Solutions to Increase B2B Sales"
          description="Wholesalers and distributors use our online suite of tools to increase their sales to their wholesale customers. Engage existing customers and find new retailers."
        />

        <HeroPanel>
          <Grid>
            <Row>
              <Col md={12}>
                <div className={`page-header ${styles.pageHeader}`}>
                  <h1>
                    Become the brand your retailers love to order from
                    <small>
                      Increase your B2B sales by bringing the B2C shopping experience to your wholesale customers
                    </small>
                  </h1>
                </div>
                <div className={styles.videoDiv} onClick={() => this.setState({showVideo: true})} >
                  <i className={`fa fa-play-circle-o fa-4x ${styles.playButton}`} aria-hidden="true"></i>
                  Watch a short video to see how OrderCircle can help!
                </div>
                <FreeTrialButton />
              </Col>
            </Row>
          </Grid>
        </HeroPanel>

        <ExistingCustomers />

        <ScreenshotInfoPanel transparent img={dashboardImg} buttonHref="/b2b-portal">
          <h2>Accept and Manage Your Wholesale Orders Online</h2>
          <h3>
            Give your customers the modern eCommerce experience they expect from an online wholesaler
            <br /><br />
            Quickly add products, customize price lists and invite your customers to start placing their orders online
          </h3>
        </ScreenshotInfoPanel>

        <ImageInfoPanel img={callCenterImg} buttonHref="/wholesale-call-center" rightImage>
          <h2>Outsource your inbound customer service and wholesale inquiries</h2>
          <h3>
            Cut costs with our sales rep on demand service instead of hiring in house staff
            <br /><br />
            Our US based sales staff will be available full time on your dedicated phone line to handle all incoming calls
            </h3>
        </ImageInfoPanel>

        <ImageInfoPanel transparent buttonHref="/sales-reps-on-demand" img={ordercirclePremiumImg}>
          <h2>Find new wholesale customers for your business</h2>
          <h3>
            Leverage our vast network of retailers to find new territories and new leads
            <br /><br />
            Our B2B specialist works with you directly to develop an outbound marketing campaign which we execute on your behalf
          </h3>
        </ImageInfoPanel>

        <div className={styles.callbackContainer}>
          <CallbackForm />
        </div>

        <ImageInfoPanel img={customSolutionImg} rightImage>
          <h2>Need a custom wholesale solution?</h2>
          <h3>
            If your company has special requirement for your wholesale eCommerce portal let us know
            <br /><br />
            We'll work with you to build out custom features to meet your wholesale needs
          </h3>
        </ImageInfoPanel>

        <ScheduleDemo />

        <Modal
          show={this.state.showVideo}
          onHide={() => this.setState({showVideo: false})}
          bsSize="lg"
        >
          <Modal.Header
            closeButton />

          <Modal.Body>
            <iframe
              src="https://player.vimeo.com/video/107058442?byline=0&portrait=0&autoplay=1"
              width="100%"
              height="460"
              frameBorder="0"
              webkitallowfullscreen
              mozallowfullscreen
              allowFullScreen
            />
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}
