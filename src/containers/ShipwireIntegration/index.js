import React, { Component } from 'react';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Button from 'react-bootstrap/lib/Button';
import Image from 'react-bootstrap/lib/Image';
import Well from 'react-bootstrap/lib/Well';
import Modal from 'react-bootstrap/lib/Modal';
import Helmet from 'components/Helmet';

import CallbackForm from 'components/CallbackForm';
import ScreenshotInfoPanel from 'components/ScreenshotInfoPanel';
import ImageInfoPanel from 'components/ImageInfoPanel';
import ScheduleDemo from 'components/ScheduleDemo';
import HeroPanel from 'components/HeroPanel';
import FreeTrialButton from 'components/FreeTrialButton';

const shipwireFulfillmentImg = require('../../../static/images/shipwire-fulfillment.png');
const shipwireItemSyncImg = require('../../../static/images/shipwire-item-sync.png');
const shipwireOrderUpdateImg = require('../../../static/images/shipwire-order-updates.png');

const shipwireTopLogo = require('../../../static/images/logos/shipwire-logo-small.png');

const styles = require('./style.scss');

export default class ShipwireIntegration extends Component {

  render() {
    return (
      <div>
        <Helmet
          title="Wholesale Order Acceptance and Fulfillment with Shipwire"
          description="Provide an online cart for your wholesale customers to log in and place orders. Orders sync to Shipwire to allow you to automate shipping and fulfillment."
        />

        <HeroPanel blue>
          <Grid>
            <Row>
              <Col md={12}>
                <Image src={shipwireTopLogo} />
                <div className={`page-header ${styles.pageHeader}`}>
                  <h1>
                    Complete wholesale automation with Shipwire
                    <small>
                      Never worry about owning or managing your warehouse ever again. Outsource your fulfillment to Shipwire to completely streamline your wholesale business.
                    </small>
                  </h1>
                </div>
                <FreeTrialButton />
              </Col>
            </Row>
          </Grid>
        </HeroPanel>

        <ImageInfoPanel img={shipwireFulfillmentImg} rightImage>
          <h2>B2B order management with shipping and fulfullment made easy</h2>
          <h3>
            Give your wholesale customers the modern eCommerce experience they expect
            <br /><br />
            Your B2B customer can log in and place orders online in a self service eCommerce portal
            <br /><br />
            Save time and money when your order acceptance is automatically synchronized with your fulfillment and shipping channels
          </h3>
        </ImageInfoPanel>

        <ImageInfoPanel img={shipwireOrderUpdateImg}>
          <h2>Automatic order status and shipping updates</h2>
          <h3>
            Eliminate unnecessary back and forth calls with your customers regarding order status
            <br /><br />
            You and your customers will get real time order and shipping notifications directly to your inbox
          </h3>
        </ImageInfoPanel>

        <ImageInfoPanel img={shipwireItemSyncImg} rightImage>
          <h2>Synchronize your items and customers between Shipwire and OrderCircle</h2>
          <h3>
            Save time by importing your customer data and SKUs from Shipwire into your store on OrderCircle
            <br /><br />
            Your data stays in sync continously, allowing you to focus on making excellent product and spent less time and money on data entry
          </h3>
        </ImageInfoPanel>

        <div className={styles.callbackContainer}>
          <CallbackForm />
        </div>

      </div>
    );
  }
}

