import React, { Component, PropTypes } from 'react';
import { IndexLink, Link } from 'react-router';
import { LinkContainer } from 'react-router-bootstrap';
import { metrics } from 'react-metrics';

const jquery = require('jquery');

import Image from 'react-bootstrap/lib/Image';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Navbar from 'react-bootstrap/lib/Navbar';
import Nav from 'react-bootstrap/lib/Nav';
import NavItem from 'react-bootstrap/lib/NavItem';
import MenuItem from 'react-bootstrap/lib/MenuItem';
import NavDropdown from 'react-bootstrap/lib/NavDropdown';
import Helmet from 'react-helmet';
import config from '../../config';
import metricsConfig from 'helpers/metrics';

const smallLogoImg = require('../../../static/images/logos/small-logo.png');

const aboutMenuItems = [
  {
    title: 'Why OrderCircle?',
    dest: '/why-ordercircle',
  },
  {
    title: 'Pricing',
    dest: '/pricing',
  },
  {
    title: 'FAQ',
    dest: '/faq',
  },
  {
    title: 'Contact Us',
    dest: '/contact'
  }
];

const productMenuItems = [
  {
    title: 'B2B Portal',
    dest: '/b2b-portal',
  },
  {
    title: 'Wholesale Call Center',
    dest: '/wholesale-call-center',
  },
  {
    title: 'Sales Rep On Demand',
    dest: '/sales-reps-on-demand',
  },
  {
    title: 'Integrations',
    dest: '/integrations'
  },
];

const industriesMenuItems = [
  {
    title: 'Coffee',
    dest: '/',
  },
  {
    title: 'Eyewear',
    dest: '/',
  },
  {
    title: 'Fashion',
    dest: '/',
  },
  {
    title: 'Food & Beverage',
    dest: '/'
  },
  {
    title: 'Footwear',
    dest: '/'
  },
  {
    title: 'Gift & Homewares',
    dest: '/'
  },
  {
    title: 'Healthcare',
    dest: '/'
  },
  {
    title: 'Music Supplies',
    dest: '/'
  },
  {
    title: 'Outdoor & Sporting Goods',
    dest: '/'
  },
  {
    title: 'Toys, Baby & Kids',
    dest: '/'
  },
  {
    title: 'Vaping & Tobacco',
    dest: '/'
  },
];


@metrics(metricsConfig)
export default class App extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired
  };

  static contextTypes = {
    store: PropTypes.object.isRequired
  };

  componentDidMount() {
    const ScrollDepth = require('scroll-depth');
    const jQuery = require('jquery');
    jQuery.scrollDepth();
  }

  menuItemClicked() {
    jquery('.open.dropdown').click();
  }

  render() {
    const { location } = this.props;
    const styles = require('./App.scss');

    return (
      <div className={styles.app}>
        <Helmet {...config.app.head}/>
        <Navbar>
          <Navbar.Header>
            <Navbar.Brand>
              <IndexLink to="/" className={styles.logoLink}>
                <Image src={smallLogoImg} className={styles.smallLogo} />
                OrderCircle
              </IndexLink>
            </Navbar.Brand>
            <Navbar.Toggle/>
          </Navbar.Header>

          <Navbar.Collapse eventKey={0}>
            <Nav className={styles.navRight} navbar pullRight>
              <li role="presentation">
                <a href="tel:+18886678630">Call us at +1 (888) 667-8630</a>
              </li>
              <a
                href="https://calendly.com/ordercircle"
                target="_blank"
                className="btn btn-secondary btn-secondary-orange demo-nav-btn navbar-btn margin-md-right"
                data-metrics-event-name="Navbar Schedule Demo Clicked"
              >
                Request Live Demo
              </a>
              <NavDropdown eventKey={1} title={"Solutions"} id="solutionsDropdown" className={styles.solutionsDropdown}>
                <Col md={5} className={styles.aboutDropdownMenuColumn}>
                  <MenuItem className={styles.menuHeader} header>About</MenuItem>
                  {
                    aboutMenuItems.map((item, index) => (
                      <LinkContainer key={index} to={item.dest}>
                        <MenuItem onSelect={this.menuItemClicked} className={styles.menuItem}>{item.title}</MenuItem>
                      </LinkContainer>
                    ))
                  }
                </Col>
                <Col md={7} className={styles.productsDropdownMenuColumn}>
                  <MenuItem className={styles.menuHeader} header>Products</MenuItem>
                  {
                    productMenuItems.map((item, index) => (
                      <LinkContainer key={index} to={item.dest}>
                        <MenuItem onSelect={this.menuItemClicked} className={styles.menuItem}>{item.title}</MenuItem>
                      </LinkContainer>
                    ))
                  }
                </Col>
              </NavDropdown>
              <NavDropdown id="industriesDropdown" className={styles.industriesDropdown} eventKey={1} title={"Industries"}>
                {
                  industriesMenuItems.map((item, index) => (
                    <LinkContainer key={index} to={item.dest}>
                      <MenuItem className={styles.menuItem}>{item.title}</MenuItem>
                    </LinkContainer>
                  ))
                }
              </NavDropdown>

              <a
                href="/signup"
                className="btn btn-secondary navbar-btn"
                data-metrics-event-name="Navbar Signup Button Clicked"
              >
                Start Free Trial
              </a>
            </Nav>
          </Navbar.Collapse>
        </Navbar>

        <div className={styles.content}>
          {this.props.children}
        </div>
        <footer className={styles.footer}>
          <Grid>
            <Row>
              <Col sm={5}>
                OrderCircle © 2016
              </Col>
              <Col sm={7} className="text-right">
                <ul className="list-inline">
                  <li><Link to="/">Home</Link></li>
                  <li><Link to="/faq">FAQ</Link></li>
                  <li><Link to="/terms">Terms / Privacy</Link></li>
                  <li><Link to="/contact">Contact</Link></li>
                  <li><a href="http://blog.ordercircle.com">Blog</a></li>
                </ul>
              </Col>
            </Row>
            <Row>
              <Col md={3}>
                <ul className="list-unstyled">
                  <li>
                    <a href="https://heapanalytics.com/?utm_source=badge">
                      <img style={{width: 108, height: 41}} src="https://heapanalytics.com/img/badgeLight.png" alt="Heap | Mobile and Web Analytics" />
                    </a>
                  </li>
                  <li>
                    <a href="//getjaco.com" rel="nofollow" target="_blank">
                      <img src="//www.getjaco.com/images/free_with_logo/jaco-badge-white.png" alt="Jaco Analytics" />
                    </a>
                  </li>
                </ul>
              </Col>
              <Col md={6} mdOffset={3} className={`text-right ${styles.socal}`}>
                <ul className="list-inline">
                  <li>
                    <a target="_blank" href="https://www.facebook.com/ordercircle">
                      <i className="fa fa-2x fa-facebook" />
                    </a>
                  </li>
                  <li>
                    <a target="_blank" href="https://twitter.com/OrderCircle">
                      <i className="fa fa-2x fa-twitter" />
                    </a>
                  </li>
                  <li>
                    <a target="_blank" href="https://www.linkedin.com/company/ordercircle">
                      <i className="fa fa-2x fa-linkedin" />
                    </a>
                  </li>
                  <li>
                    <a target="_blank" href="https://www.youtube.com/channel/UCEtdS1ChyFv5YP2c4HKLqQg">
                      <i className="fa fa-2x fa-youtube" />
                    </a>
                  </li>
                  <li>
                    <a target="_blank" href="https://plus.google.com/+ordercircle/">
                      <i className="fa fa-2x fa-google-plus" />
                    </a>
                  </li>
                  <li>
                    <a href="mailto:info@ordercircle.com" target="_top">
                      <i className="fa fa-2x fa-envelope" />
                    </a>
                  </li>
                </ul>
              </Col>
            </Row>
          </Grid>
        </footer>
      </div>
    );
  }
}
