import React, { Component } from 'react';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Well from 'react-bootstrap/lib/Well';
import Helmet from 'components/Helmet';

import PricingTable from 'components/PricingTable';
import Faq from 'components/Faq';

export default class Pricing extends Component {
  render() {
    return (
      <Grid>
        <Helmet
          title="The Most Competitively Priced B2B E-Commerce Platform On The Market"
          description="Launch your e-commerce store for your wholesale customers in a few hours. No yearly contract, cancel whenever you want."
        />

        <Row>
          <Col md={12} className="text-center">
            <h1>
              <strong>
                Free trial. No contract. No Credit Card Required. Cancel whenever.
              </strong>
            </h1>
          </Col>
        </Row>
        <Row>
          <Col md={12}>
            <PricingTable />
          </Col>
        </Row>
        <Row>
          <Col md={12} className="margin-lg-v">
            <Well className="text-center">
              Unsure which plan is best for you? You can call our sales team at any time at 888-667-8630.
            </Well>
          </Col>
        </Row>
      </Grid>
    );
  }
}
