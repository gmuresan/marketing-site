import React, { Component } from 'react';
import Link from 'react-router/lib/Link';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Alert from 'react-bootstrap/lib/Alert';
import Helmet from 'components/Helmet';

export default class Contact extends Component {
  render() {
    return (
      <Grid>
        <Helmet
          title="7 Day Phone Support From B2B Wholesale Experts"
          description="Reach us 7 days a week for all your wholesale and b2b sales questions. Call us at 888.667.8630 to get your e-commerce questions answered by our team of b2b experts."
        />

        <Row>
          <Col md={9}>
            <h3>Send us a message</h3>
            <p>We're always happy to chat! The best way to get ahold of us is email, but if it's very urgent you can pick up the phone. Pick the contact point below that makes the most sense for the topic you want to talk about:</p>
            <p><strong>Support:</strong> help@ordercircle.com</p>
            <p><strong>Prospective Customers:</strong> sales@ordercircle.com</p>
            <p><strong>Press / Biz Dev:</strong> info@ordercircle.com</p>
          </Col>
          <Col md={3}>
            <h4>By Phone</h4>
            <p>1-888-667-8630</p>
            <Alert bsStyle="info">
              <p><strong>Need Help Now?</strong></p>
              <Link to="/faq">
                We've answered a lot of questions on our frequently asked questions page.
              </Link>
            </Alert>
          </Col>
        </Row>
      </Grid>
    );
  }
}
