import React, { Component } from 'react';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Button from 'react-bootstrap/lib/Button';
import Image from 'react-bootstrap/lib/Image';
import Well from 'react-bootstrap/lib/Well';
import Modal from 'react-bootstrap/lib/Modal';
import Helmet from 'components/Helmet';
import { Link } from 'react-router';

import CallbackForm from 'components/CallbackForm';
import ScreenshotInfoPanel from 'components/ScreenshotInfoPanel';
import ImageInfoPanel from 'components/ImageInfoPanel';
import ScheduleDemo from 'components/ScheduleDemo';
import HeroPanel from 'components/HeroPanel';
import FreeTrialButton from 'components/FreeTrialButton';

const shopifyLogoSmall = require('../../../static/images/logos/shopify-logo-small.png');
const shipstationLogoSmall = require('../../../static/images/logos/shipstation-logo-small.png');
const shipwireLogoSmall = require('../../../static/images/logos/shipwire-logo-small.png');
const quickbooksLogoSmall = require('../../../static/images/logos/quickbooks-logo-small.png');

const shopifyLogo = require('../../../static/images/logos/shopify-logo.png');
const shipstationLogo = require('../../../static/images/logos/shipstation-logo.png');
const shipwireLogo = require('../../../static/images/logos/shipwire-logo.png');
const quickbooksLogo = require('../../../static/images/logos/quickbooks-logo.png');

const styles = require('./style.scss');

const topLogos = [
  {
    img: quickbooksLogoSmall,
    href: '/quickbooks-integration'
  },
  {
    img: shopifyLogoSmall,
    href: '/shopify-integration'
  },
  {
    img: shipwireLogoSmall,
    href: '/shipwire-integration'
  },
  {
    img: shipstationLogoSmall,
    href: '/shipstation-integration'
  }
];

export default class Integrations extends Component {

  render() {
    return (
      <div>
        <Helmet
          title="Integrate Your Wholesale Business With Your B2C Channel"
          description="Your wholesale and distribution channels shouldn't be separated from the rest of your business. Easily integrate your b2b sales with our suite of solutions."
        />

        <HeroPanel>
          <Grid>
            <Row>
              <Col md={12}>
                <div className={`page-header ${styles.pageHeader}`}>
                  <h1>
                    Wholesale should be integrated to your B2C channel
                    <small>
                      Save time and stay organized by connecting your existing eCommerce tools with your wholesale order acceptance and management
                    </small>
                  </h1>
                </div>
                <div className={styles.topLogoContainer}>
                  {
                    topLogos.map((image, index) => (
                      <Link to={image.href} key={index}>
                        <img src={image.img} className={styles.topLogo} />
                      </Link>
                    ))
                  }
                </div>
                <FreeTrialButton />
              </Col>
            </Row>
          </Grid>
        </HeroPanel>

        <ImageInfoPanel img={quickbooksLogo} rightImage buttonHref="/quickbooks-integration">
          <h2>Easy accounting and invoicing with QuickBooks</h2>
          <h3>
            Automatically create customer invoices when you approve a wholesale order
            <br /><br />
            OrderCircle fills in customer info and line items to save you hours every month
          </h3>
        </ImageInfoPanel>

        <ImageInfoPanel transparent img={shopifyLogo} buttonHref="/shopify-integration" containerClass={styles.shopifyContainer}>
          <h2>Synchronize your B2B and B2C channels</h2>
          <h3>
            Shopify is great for B2C but is very messy when it comes to wholesale
            <br /><br />
            Instantly import your existing items from Shopify and automatically synchronize inventory levels
          </h3>
        </ImageInfoPanel>

        <ImageInfoPanel img={shipstationLogo} rightImage buttonHref="/shipstation-integration">
          <h2>Eliminate shipping headaches with ShipStation</h2>
          <h3>
            Get real time shipping rates across multiple carriers
            <br /><br />
            Automatically notify your customers when orders go out and shipments are updated
          </h3>
        </ImageInfoPanel>

        <ImageInfoPanel transparent img={shipwireLogo} buttonHref="/shipwire-integration" containerClass={styles.shipwireContainer}>
          <h2>Automate your shipping and fulfillment to reduce costs</h2>
          <h3>
            Eliminate the need to operate your own warehouse by outsourcing to Shipwire
            <br /><br />
            Use OrderCircle to accept and manage wholesale orders while Shipwire takes care of shipping and fulfillment
          </h3>
        </ImageInfoPanel>

        <div className={styles.callbackContainer}>
          <CallbackForm />
        </div>

      </div>
    );
  }
}

